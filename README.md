Implementation of Probabilistic Business Constraints Monitoring
====================

The implementation is packaged into a standalone file LTL2Automaton.jar. It takes the following parameters:
	
	usage: java -jar .\ProbMonitor.jar
	-declare           used if the model contains Declare constraints instead of LTL formulas
	-l,--log <arg>     input log file
	-m,--model <arg>   input model path
	-maxTraces         maximum number of traces to process (default is to process all traces)

Implementation is done in Java 1.8

The visualisation of the results is implemented in a larger application called RuM which is located here: https://bitbucket.org/doorless1634/thesis/src/alman-thesis/

Example command to run the application from the root folder of the repository: ``java -jar .\ProbMonitor.jar -declare -l '.\BPI_2018\0_event_logs\BPI Challenge 2018 - year 2015.xes' -m .\BPI_2018\2_EMD_performance\ful_scenario_4.decl -maxTraces 100``.

If multiple Java installations are present, then it may be necessary to run the application explicitly with Java 1.8. For example: ``"C:\Program Files\Java\jdk1.8.0_261\bin\java.exe" -jar .\ProbMonitor.jar <parameters>``, where ``jdk1.8.0_261`` is the installation directory of Java 1.8.


Implementation of EMD based conformance checking
====================

The implementation is packaged into a standalone file LTL2Automaton.jar. It takes the following parameters:
	
	usage: java -jar .\EMDConformance.jar
	-declare           used if the model contains Declare constraints instead of LTL formulas
	-l,--log <arg>     input log path
	-m,--model <arg>   input model path

Requires Java 1.8 to run

Example command to run the application from the root folder of the repository: ``java -jar .\EMDConformance.jar -declare -l '.\BPI_2018\0_event_logs\BPI Challenge 2018 - year 2015.xes' -m .\BPI_2018\2_EMD_performance\ful_scenario_4.decl``.

Note that using larger models may require more memory than is allocated by the Java VM by default. In that case an additional parameter ``-Xmx10g`` may be necessary. For example: ``java -Xmx10g -jar .\EMDConformance.jar <parameters>``.

If multiple Java installations are present, then it may be necessary to run the application explicitly with Java 1.8. For example: ``"C:\Program Files\Java\jdk1.8.0_261\bin\java.exe" -jar .\EMDConformance.jar <parameters>``, where ``jdk1.8.0_261`` is the installation directory of Java 1.8.

