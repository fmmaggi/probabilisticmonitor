package declare;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeclareUtils {

	private DeclareUtils() {
		//Private constructor to avoid unnecessary instantiation of the class
	}

	//Returns a generic LTL formula for a given Declare template (formulas are modified to work with FLLOAT)
	public static String getGenericLtlFormula(DeclareTemplate declareTemplate) {
		String formula = "";
		switch (declareTemplate) {
			case Absence:
				formula = "!( <> ( \"A\" ) )";
				break;
			case Absence2:
				formula = "! ( <> ( ( \"A\" && X(<>(\"A\")) ) ) )";
				break;
			case Absence3:
				formula = "! ( <> ( ( \"A\" &&  X ( <> ((\"A\" &&  X ( <> ( \"A\" ) )) ) ) ) ))";
				break;
			case Alternate_Precedence:
				formula = "(((( !(\"B\") U \"A\") || []( !(\"B\"))) && []((\"B\" ->( (!(X(\"A\")) && !(X(!(\"A\"))) ) || X((( !(\"B\") U \"A\") || []( !(\"B\")))))))) && (  ! (\"B\" ) || (!(X(\"A\")) && !(X(!(\"A\"))) ) ))";
				break;
			case Alternate_Response:
				formula = "( []( ( \"A\" -> X(( (! ( \"A\" )) U \"B\" ) )) ) )";
				break;
			case Alternate_Succession:
				formula = "( []((\"A\" -> X(( !(\"A\") U \"B\")))) && (((( !(\"B\") U \"A\") || []( !(\"B\"))) && []((\"B\" ->( (!(X(\"A\")) && !(X(!(\"A\"))) ) || X((( !(\"B\") U \"A\") || []( !(\"B\")))))))) && (  ! (\"B\" ) || (!(X(\"A\")) && !(X(!(\"A\"))) ) )))";
				break;
			case Chain_Precedence:
				formula = "[]( ( X( \"B\" ) -> \"A\") ) && (  ! (\"B\" ) || (!(X(\"A\")) && !(X(!(\"A\"))) ) )";
				break;
			case Chain_Response:
				formula = "[] ( ( \"A\" -> X( \"B\" ) ) )";
				break;
			case Chain_Succession:
				//This formula does not work correctly with the FLLOAT automata library (incorrect results)
				formula = "( []( ( \"A\" -> X( \"B\" ) ) )) && ([]( ( X( \"B\" ) ->  \"A\") ) && (  ! (\"B\" ) || (!(X(\"A\")) && !(X(!(\"A\"))) ) ) )";
				break;
			case Choice:
				formula = "(  <> ( \"A\" ) || <>( \"B\" )  )";
				break;
			case CoExistence:
				formula = "( ( <>(\"A\") -> <>( \"B\" ) ) && ( <>(\"B\") -> <>( \"A\" ) )  )";
				break;
			case End:
				//formula = "<>((A) && ( ! (X(A ||  (!(A))))))";
				formula = "( <> ( \"A\" && !X( \"A\" U ( !\"A\" ) ) ) )";
				break;
			case Exactly1:
				formula = "(  <> (\"A\") && ! ( <> ( ( \"A\" && X(<>(\"A\")) ) ) ) )";
				break;
			case Exactly2:
				formula = "( <> (\"A\" && (\"A\" -> (X(<>(\"A\"))))) &&  ! ( <>( \"A\" && (\"A\" -> X( <>( \"A\" && (\"A\" -> X ( <> ( \"A\" ) ))) ) ) ) ) )";
				break;
			case Exclusive_Choice:
				formula = "(  ( <>( \"A\" ) || <>( \"B\" )  )  && !( (  <>( \"A\" ) && <>( \"B\" ) ) ) )";
				break;
			case Existence:
				formula = "( <> ( \"A\" ) )";
				break;
			case Existence2:
				formula = "<> ( ( \"A\" && X(<>(\"A\")) ) )";
				break;
			case Existence3:
				formula = "<>( \"A\" && X(  <>( \"A\" && X( <> \"A\" )) ))";
				break;
			case Init:
				formula = "( \"A\" )";
				break;
			case Not_Chain_Precedence:
				formula = "[] ( \"A\" -> !( X ( \"B\" ) ) )";
				break;
			case Not_Chain_Response:
				formula = "[] ( \"A\" -> !( X ( \"B\" ) ) )";
				break;
			case Not_Chain_Succession:
				formula = "[]( ( \"A\" -> !(X( \"B\" ) ) ))";
				break;
			case Not_CoExistence:
				formula = "(<>( \"A\" )) -> (!(<>( \"B\" )))";
				break;
			case Not_Precedence:
				formula = "[] ( \"A\" -> !( <> ( \"B\" ) ) )";
				break;
			case Not_Responded_Existence:
				formula = "(<>( \"A\" )) -> (!(<>( \"B\" )))";
				break;
			case Not_Response:
				formula = "[] ( \"A\" -> !( <> ( \"B\" ) ) )";
				break;
			case Not_Succession:
				formula = "[]( ( \"A\" -> !(<>( \"B\" ) ) ))";
				break;
			case Precedence:
				formula = "( ! (\"A\" ) U \"B\" ) || ([](!(\"A\"))) && (  ! (\"A\" ) || (!(X(\"B\")) && !(X(!(\"B\"))) ) )";
				break;
			case Responded_Existence:
				formula = "(( ( <>( \"A\" ) -> (<>( \"B\" ) )) ))";
				break;
			case Response:
				formula = "( []( ( \"A\" -> <>( \"B\" ) ) ))";
				break;
			case Succession:
				formula = "(( []( ( \"A\" -> <>( \"B\" ) ) ))) && (( ! (\"B\" ) U \"A\" ) || ([](!(\"B\"))) && (  ! (\"B\" ) || (!(X(\"A\")) && !(X(!(\"A\"))) ) )   )";
				break;
			default:
				break;
		}
		return formula;
	}

	//Creates a single Declare constraint object from Declare constraint string
	public static DeclareConstraint readConstraintString(String constraintString) {
		DeclareTemplate template = null;
		String activationActivity = "";
		String targetActivity = "";

		Matcher mBinary = Pattern.compile("(.*)\\[(.*), (.*)\\]").matcher(constraintString);
		Matcher mUnary = Pattern.compile(".*\\[(.*)\\]").matcher(constraintString);

		//Processing the constraint
		if(mBinary.find()) { //Binary constraints
			template = DeclareTemplate.getByTemplateName(mBinary.group(1));
			if(template.getReverseActivationTarget()) {
				targetActivity = mBinary.group(2);
				activationActivity = mBinary.group(3);
			}
			else {
				activationActivity = mBinary.group(2);
				targetActivity = mBinary.group(3);
			}
			constraintString = mBinary.group(1) + "[" + mBinary.group(2) + ", " + mBinary.group(3) + "]";
		} else if(mUnary.find()) { //Unary constraints
			String templateString = mUnary.group(0).substring(0, mUnary.group(0).indexOf("[")); //TODO: Should be done more intelligently
			template = DeclareTemplate.getByTemplateName(templateString);
			activationActivity = mUnary.group(1);
			constraintString = template + "[" + mUnary.group(1) + "]";
		}

		return new DeclareConstraint(constraintString, template, activationActivity, targetActivity);
	}
}
