package org.processmining.onlinedeclareanalyzer.lpsolver.ast;

import java.util.ArrayList;

/**
 * <expression>::=<term>{<or><term>}
 * <term>::=<factor>{<and><factor>}
 * <factor>::=<constant>|<not><factor>|(<expression>)
 * <constant>::= false|true
 * <or>::='|'
 * <and>::='&'
 * <not>::='!'
 */
public interface BooleanExpression {
	ArrayList<ArrayList<BooleanExpression>> interpret();
	ArrayList<ArrayList<BooleanExpression>> invertInterpret();
	ArrayList<String> traverse();
	ArrayList<String> invertTraverse();
}
