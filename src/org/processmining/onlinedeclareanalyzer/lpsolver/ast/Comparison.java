package org.processmining.onlinedeclareanalyzer.lpsolver.ast;

/**
 * Created by Ubaier on 02/04/2016.
 */
public abstract class Comparison implements BooleanExpression {
    protected BooleanExpression left, right;
    protected String symbol;

    public void setLeft(BooleanExpression left) {
        this.left = left;
    }

    public void setRight(BooleanExpression right) {
        this.right = right;
    }

    public void setSymbol(String symbol){
        this.symbol = symbol;
    }

    public BooleanExpression getLeft() {
        return left;
    }

    public BooleanExpression getRight() {
        return right;
    }

    public String getSymbol() {
        return symbol;
    }
}
