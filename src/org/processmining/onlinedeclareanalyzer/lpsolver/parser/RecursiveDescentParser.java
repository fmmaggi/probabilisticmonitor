package org.processmining.onlinedeclareanalyzer.lpsolver.parser;


import org.processmining.onlinedeclareanalyzer.lpsolver.ast.BooleanExpression;
import org.processmining.onlinedeclareanalyzer.lpsolver.ast.nonterminal.*;
import org.processmining.onlinedeclareanalyzer.lpsolver.ast.terminal.TerminalValue;
import org.processmining.onlinedeclareanalyzer.lpsolver.lexer.Lexer;
import org.processmining.framework.util.Pair;

public class RecursiveDescentParser {
    private Lexer lexer;
    private Pair<Integer, String> symbol;
    private BooleanExpression root;

    private TerminalValue e = new TerminalValue();

    public RecursiveDescentParser(Lexer lexer) {
        this.lexer = lexer;
    }

    public BooleanExpression build() {
        expression();
        return root;
    }

    private void expression() {
        term();
        while (symbol.getFirst() == Lexer.OR) {
            Or or = new Or();
            or.setLeft(root);
            term();
            or.setRight(root);
            root = or;
        }
    }

    private void term() {
        factor();
        if (symbol.getFirst() == Lexer.GT) {
            while (symbol.getFirst() == Lexer.GT) {
                Co co = new Co();
                co.setLeft(root);
                co.setSymbol(Lexer.GT_LITERAL);
                factor();
                co.setRight(root);
                root = co;
            }
        }
        if (symbol.getFirst() == Lexer.LT) {
            while (symbol.getFirst() == Lexer.LT) {
                Co co = new Co();
                co.setLeft(root);
                co.setSymbol(Lexer.LT_LITERAL);
                factor();
                co.setRight(root);
                root = co;
            }
        }
        if (symbol.getFirst() == Lexer.GTE) {
            while (symbol.getFirst() == Lexer.GTE) {
                Co co = new Co();
                co.setLeft(root);
                co.setSymbol(Lexer.GTE_LITERAL);
                factor();
                co.setRight(root);
                root = co;
            }
        }
        if (symbol.getFirst() == Lexer.LTE) {
            while (symbol.getFirst() == Lexer.LTE) {
                Co co = new Co();
                co.setLeft(root);
                co.setSymbol(Lexer.LTE_LITERAL);
                factor();
                co.setRight(root);
                root = co;
            }
        }

        if (symbol.getFirst() == Lexer.EQUAL) {
            while (symbol.getFirst() == Lexer.EQUAL) {
                Co co = new Co();
                co.setLeft(root);
                co.setSymbol(Lexer.EQUAL_LITERAL);
                factor();
                co.setRight(root);
                root = co;
            }
        }

        if (symbol.getFirst() == Lexer.NOT_EQUAL) {
            while (symbol.getFirst() == Lexer.NOT_EQUAL) {
                Co co = new Co();
                co.setLeft(root);
                co.setSymbol(Lexer.NE_LITERAL);
                factor();
                co.setRight(root);
                root = co;
            }
        }

        if (symbol.getFirst() == Lexer.AND) {
            while (symbol.getFirst() == Lexer.AND) {
                And and = new And();
                and.setLeft(root);
                factor();
                and.setRight(root);
                root = and;
            }
        }

        if (symbol.getFirst() == Lexer.PLUS) {
            while (symbol.getFirst() == Lexer.PLUS) {
                Lexp lexp = new Lexp();
                lexp.setLeft(root);
                lexp.setSymbol(Lexer.PLUS_LITERAL);
                factor();
                lexp.setRight(root);
                root = lexp;
            }
        }

        if (symbol.getFirst() == Lexer.MINUS) {
            while (symbol.getFirst() == Lexer.MINUS) {
                Lexp lexp = new Lexp();
                lexp.setLeft(root);
                lexp.setSymbol(Lexer.MINUS_LITERAL);
                factor();
                lexp.setRight(root);
                root = lexp;
            }
        }
    }


    private void factor() {
        symbol = lexer.nextSymbol();
        if (symbol.getFirst() == Lexer.VARIABLE) {
            e = new TerminalValue(symbol.getSecond());
            root = e;
            symbol = lexer.nextSymbol();
        } else if (symbol.getFirst() == Lexer.NOT) {
            Not not = new Not();
            factor();
            not.setChild(root);
            root = not;
        } else if (symbol.getFirst() == Lexer.LEFT) {
            expression();
            symbol = lexer.nextSymbol(); // we don't care about ')'
        } else {
            throw new RuntimeException("Expression Malformed: unknown Symbol: " + symbol.getFirst());
        }
    }
}
