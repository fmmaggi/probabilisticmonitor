package org.processmining.ltl2automaton.plugins.automaton;

import java.util.ArrayList;

public class TreeAutomaton {
	ArrayList<TreeAutomatonTransition> transitions = new ArrayList<TreeAutomatonTransition>();
	ArrayList<TreeAutomatonState> states = new ArrayList<TreeAutomatonState>();
	int count = 0;

	public ArrayList<TreeAutomatonTransition> getTransitions() {
		return transitions;
	}

	public void addTransitions(TreeAutomatonTransition transition) {
		transitions.add(transition);
	}

	public ArrayList<TreeAutomatonState> getStates() {
		return states;
	}

	public void addState(TreeAutomatonState state) {
		states.add(state);
	}
	
	
	

}
