package rum.server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.deckfour.xes.in.XesXmlParser;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

import main.Boundaries;
import main.BuildMonitor;
import main.VisualizationItem;

public class Server {
	private static ServerSocket server;
	private static int port = 9875;
	private static XesXmlParser parser = new XesXmlParser();
	private static HashMap<String,ArrayList<VisualizationItem>> traceItems = new HashMap<String,ArrayList<VisualizationItem>>();
	private static HashMap<VisualizationItem, ArrayList<ArrayList<String>>> mholds = new HashMap<VisualizationItem, ArrayList<ArrayList<String>>>();

	public static void main(String[] args) throws Exception {
		server = new ServerSocket(port);
		String letto = null;
		ArrayList<String> model = new ArrayList<String>();

		System.out.println("\nWaiting for client request");
		Socket socket = server.accept();
		letto = receive(socket);

		File temp = File.createTempFile("tempfile", ".tmp");
		BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
		while(!letto.equals("</model>")){
			model.add(letto);
			bw.write(letto + System.lineSeparator());
			socket = server.accept();
			letto = receive(socket);
		}
		System.out.println("model: " + model.toString());
		bw.close();
		
		Thread.sleep(1000);
		
		socket = server.accept();
		letto = receive(socket); //Should be the first event

		try {
			while (!letto.equalsIgnoreCase("exit")) {	
				System.out.println("Received Message: " + letto);

				SAXBuilder parserSax = new SAXBuilder();
				Document mess = parserSax.build(new InputSource(new StringReader(letto)));
				Element rootMess = mess.getRootElement();

				String event = rootMess.getChild("WorkflowModelElement").getText();
				String piID = rootMess.getChild("ProcessInstanceID").getText();
				long timestamp = new Long(rootMess.getChild("Timestamp").getText()).longValue();
				
				//From BuildMonitor.main (modified to hopefully support processing multiple traces)
				if (traceItems.get(piID)==null) {
					traceItems.put(piID, BuildMonitor.getMonitoredItems(temp.getAbsolutePath(), false));
					BuildMonitor.printItems(traceItems.get(piID));
					BuildMonitor.initializeItems(traceItems.get(piID));
					
				}				
				//From BuildMonitor.main
				ArrayList<VisualizationItem> possSat = new ArrayList<VisualizationItem>(); 
				ArrayList<VisualizationItem> possViol = new ArrayList<VisualizationItem>(); 
				ArrayList<VisualizationItem> sat = new ArrayList<VisualizationItem>();
				ArrayList<VisualizationItem> viol = new ArrayList<VisualizationItem>();
				System.out.println("@@@@@@@@@@@@@");
				System.out.println("Trace: " + piID + " - Event: "+event);
				
				String fluents = "[";
				for(VisualizationItem item : traceItems.get(piID)){
					boolean truthValueChanged = false;
					if(event.equals("complete")){
						String truthValue = item.getTruthValue();
						if(truthValue.equals("sat")){
							System.out.println("Variable: "+item.getVariable()+" sat");
							sat.add(item);		
						}
						if(truthValue.equals("poss.sat")){
							System.out.println("Variable: "+item.getVariable()+" sat");
							sat.add(item);		
							mholds.get(item).get(mholds.get(item).size()-1).set(2, Long.toString(timestamp));
							ArrayList<String> al = new ArrayList<String>();
							al.add("sat");
							al.add(Long.toString(timestamp));
							al.add("inf");
							mholds.get(item).add(al);
						}
						if(truthValue.equals("viol")){
							System.out.println("Variable: "+item.getVariable()+" viol");
							viol.add(item);		
						}
						if(truthValue.equals("poss.viol")){
							System.out.println("Variable: "+item.getVariable()+" viol");
							viol.add(item);
							mholds.get(item).get(mholds.get(item).size()-1).set(2, Long.toString(timestamp));
							ArrayList<String> al = new ArrayList<String>();
							al.add("viol");
							al.add(Long.toString(timestamp));
							al.add("inf");
							mholds.get(item).add(al);
						}
					}else{
						String previousTruthValue = item.getTruthValue();
						String truthValue = BuildMonitor.fireEventOnItem(event, item);
						if (!previousTruthValue.equals(truthValue)) {
							truthValueChanged=true;
						}
						System.out.println("Variable: "+item.getVariable()+" "+truthValue);
						if(truthValue.equals("sat")){
							sat.add(item);		
						}
						if(truthValue.equals("poss.sat")){
							possSat.add(item);
						}
						if(truthValue.equals("viol")){
							viol.add(item);
						}
						if(truthValue.equals("poss.viol")){
							possViol.add(item);
						}
					}
					
					if (mholds.get(item)==null) {
						mholds.put(item, new ArrayList<ArrayList<String>>());
						ArrayList<String> al = new ArrayList<String>();
						al.add(item.getTruthValue());
						al.add(Long.toString(timestamp));
						al.add("inf");
						mholds.get(item).add(al);
					} else if (truthValueChanged) {
						mholds.get(item).get(mholds.get(item).size()-1).set(2, Long.toString(timestamp));
						ArrayList<String> al = new ArrayList<String>();
						al.add(item.getTruthValue());
						al.add(Long.toString(timestamp));
						al.add("inf");
						mholds.get(item).add(al);
					}
					
					for (ArrayList<String> al : mholds.get(item)) {
						if (!fluents.equals("[")) {
							fluents = fluents + ",";
						}
						fluents = fluents + "mholds_for(status(" + item.getLabel() + "," + al.get(0) + "),[" + al.get(1) + "," + al.get(2) + "])";
					}
				}
				fluents = fluents + "]";
				
				Map<String,Boundaries> currentBoundaries = BuildMonitor.getGlobalTruthValues(temp.getAbsolutePath(), possSat, possViol, sat, viol, false);

				BuildMonitor.printCurrentBoundaries(currentBoundaries);
				System.out.println("@@@@@@@@@@@@@");
				
				HashMap<String,ArrayList<Double>> currentBoundariesMap = new HashMap<String,ArrayList<Double>>();
				for (String key : currentBoundaries.keySet()) {
					Boundaries b = currentBoundaries.get(key);
					currentBoundariesMap.put(key, new ArrayList<Double>(Arrays.asList(b.getMin(), b.getMax())));
				}
				

				Map<String, Object> analysis = new HashMap<String,Object>();
				analysis.put("positive", "");
				analysis.put("negative", "");
				analysis.put("fluents", fluents);
				analysis.put("currentBoundaries", currentBoundariesMap);

//				HashMap<String,Object> results = new HashMap<String,Object>();
//				results.put("analysis", analysis);
//				results.put("queryProbability", 0.5);
				
				send(socket, analysis);
				System.out.println("Sending analysis: " + analysis);
				socket = server.accept();
				letto = receive(socket);
			}

			server.close();
			System.out.println("Shutting down now");
			System.exit(0);

		} catch (IOException e1) {
			e1.printStackTrace();
		}	
	}

	//Get Trace and/or model from client
	private static String receive(Socket socket) throws IOException, ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
		return (String) ois.readObject();
	}

	private static void send(Socket socket, Map<String, Object> msg) throws IOException {
		ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
		oos.writeObject(msg);
	}
}
