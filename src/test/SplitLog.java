package test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.in.XesXmlGZIPParser;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeTimestampImpl;
import org.deckfour.xes.model.impl.XLogImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.deckfour.xes.out.XesXmlSerializer;

public class SplitLog {

	public static void main(String[] args) {

		//Parameters
		long splitSizeInMonths = 1;
		String outputFolder = "BPI_2012_test/split_log/";


		//Loading the log file
		XLog log = null;
		File inputLogFile = new File("BPI_2012_test/original_log/financial_log.xes.gz");
		XesXmlGZIPParser parser = new XesXmlGZIPParser();
		if(parser.canParse(inputLogFile)){
			try {
				log = parser.parse(inputLogFile).get(0);
			} catch (Exception e) {
				System.out.println("Unable to load event log: " + inputLogFile.getAbsolutePath());
			}
		}


		//Finding the time interval of the event log
		XEvent event = log.get(0).get(0);
		XAttributeTimestampImpl timestamp = (XAttributeTimestampImpl) event.getAttributes().get("time:timestamp");
		long minDate = timestamp.getValueMillis();
		long maxDate = timestamp.getValueMillis();

		for (XTrace trace : log) {
			event = trace.get(0);
			timestamp = (XAttributeTimestampImpl) event.getAttributes().get("time:timestamp");
			long timestampMillis = timestamp.getValueMillis();
			if (timestampMillis < minDate) {
				minDate = timestampMillis;
			} else if (timestampMillis > maxDate) {
				maxDate = timestampMillis;
			}
		}
		long numOfMonths = ChronoUnit.MONTHS.between(Instant.ofEpochMilli(minDate).atZone(ZoneId.systemDefault()).toLocalDate(), Instant.ofEpochMilli(maxDate).atZone(ZoneId.systemDefault()).toLocalDate())+1;
		int splits = new Double(Math.ceil(numOfMonths*1.0 / splitSizeInMonths)).intValue();

		System.out.println("Earliest trace start: " + new Date(minDate));
		System.out.println("Latest trace start: " + new Date(maxDate));
		System.out.println("Duration interval in months: " + numOfMonths);
		System.out.println("Number of splits: " + splits);



		//List for splitting the traces
		List<List<XTrace>> traceSplits = new ArrayList<List<XTrace>>(splits);
		for (int i = 0; i < splits; i++) {
			traceSplits.add(new ArrayList<XTrace>());
		}



		//Splitting the event log
		for (XTrace trace : log) {
			event = trace.get(0);
			timestamp = (XAttributeTimestampImpl) event.getAttributes().get("time:timestamp");
			long timestampMillis = timestamp.getValueMillis();

			long intervalFromMinMonth = ChronoUnit.MONTHS.between(Instant.ofEpochMilli(minDate).atZone(ZoneId.systemDefault()).toLocalDate().withDayOfMonth(1), Instant.ofEpochMilli(timestampMillis).atZone(ZoneId.systemDefault()).toLocalDate());
			int splitNr = new Double(Math.floor(intervalFromMinMonth*1.0 / splitSizeInMonths)).intValue();
			traceSplits.get(new Long(splitNr).intValue()).add(trace);
		}



		//Writing split event logs for discovery
		for (int i = 0; i < traceSplits.size(); i++) {
			XLogImpl logSplit = new XLogImpl(log.getAttributes());
			for (XTrace trace : traceSplits.get(i)) {
				XTraceImpl completeEventsTrace = new XTraceImpl(trace.getAttributes());
				for (int j = 0; j < trace.size(); j++) {
					XAttributeLiteralImpl transition = (XAttributeLiteralImpl) trace.get(j).getAttributes().get("lifecycle:transition");
					if (transition.getValue().equals("COMPLETE")) {
						String eventName = XConceptExtension.instance().extractName(trace.get(j)).replace(" ", "_").toLowerCase();
						XEvent ev = trace.get(j);
						ev.getAttributes().put("concept:name", new XAttributeLiteralImpl("concept:name", eventName));
						completeEventsTrace.add(ev);
					}
				}
				logSplit.add(completeEventsTrace);
			}
			FileOutputStream outStream;
			try {
				//TODO: Check for export differences between the generation methods
				outStream = new FileOutputStream(outputFolder + "split_discovery_" + i + ".xes");
				new XesXmlSerializer().serialize(logSplit, outStream);
				outStream.flush();
				outStream.close();
				System.out.println("Created log split: " + outputFolder + "split_discovery_" + i + ".xes (" + logSplit.size() + " traces)");
			} catch (IOException e) {
				System.out.println("Unable to create log split: " + outputFolder + "split_discovery_" + i + ".xes");
			}
		}

		//Writing split event logs for conformance checking
		for (int i = 0; i < traceSplits.size(); i++) {
			XLogImpl logSplit = new XLogImpl(log.getAttributes());
			for (XTrace trace : traceSplits.get(i)) {
				XTraceImpl completeEventsTrace = new XTraceImpl(trace.getAttributes());
				for (int j = 0; j < trace.size(); j++) {
					XAttributeLiteralImpl transition = (XAttributeLiteralImpl) trace.get(j).getAttributes().get("lifecycle:transition");
					if (transition.getValue().equals("COMPLETE")) {
						String eventName = XConceptExtension.instance().extractName(trace.get(j)).replace(" ", "_").toLowerCase();
						XEvent ev = trace.get(j);
						ev.getAttributes().put("concept:name", new XAttributeLiteralImpl("concept:name", eventName));
						completeEventsTrace.add(ev);
					}
				}
				
				//Adding event "begin"
				XFactory nxFactory = XFactoryRegistry.instance().currentDefault();
				XEvent le = nxFactory.createEvent();
				XConceptExtension nconcept = XConceptExtension.instance();
				nconcept.assignName(le, "begin");
				XLifecycleExtension nlc = XLifecycleExtension.instance();
				nlc.assignTransition(le, "complete");
				XTimeExtension ntimeExtension = XTimeExtension.instance();
				ntimeExtension.assignTimestamp(le, ntimeExtension.extractTimestamp(completeEventsTrace.get(0)).getTime() - 1);
				completeEventsTrace.add(0, le);
				
				//Adding event "complete"
				nxFactory = XFactoryRegistry.instance().currentDefault();
				le = nxFactory.createEvent();
				nconcept = XConceptExtension.instance();
				nconcept.assignName(le, "complete");
				nlc = XLifecycleExtension.instance();
				nlc.assignTransition(le, "complete");
				ntimeExtension = XTimeExtension.instance();
				ntimeExtension.assignTimestamp(le, ntimeExtension.extractTimestamp(completeEventsTrace.get(completeEventsTrace.size() - 1)).getTime() + 1);
				completeEventsTrace.add(le);

				logSplit.add(completeEventsTrace);
			}
			FileOutputStream outStream;
			try {
				//TODO: Check for export differences between the generation methods
				outStream = new FileOutputStream(outputFolder + "split_conformance_" + i + ".xes");
				new XesXmlSerializer().serialize(logSplit, outStream);
				outStream.flush();
				outStream.close();
				System.out.println("Created log split: " + outputFolder + "split_conformance_" + i + ".xes (" + logSplit.size() + " traces)");
			} catch (IOException e) {
				System.out.println("Unable to create log split: " + outputFolder + "split_conformance_" + i + ".xes");
			}
		}
	}
}
