package test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.extension.std.XLifecycleExtension;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.in.XesXmlGZIPParser;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XLogImpl;
import org.deckfour.xes.model.impl.XTraceImpl;
import org.deckfour.xes.out.XesXmlSerializer;

public class ProcessLog {

	public static void main(String[] args) {
		String inputPath = "sw_test/split_log_incremental/";
		String outputPath = "sw_test/processed_log_incremental/";

		for (File inputLogFile : new File(inputPath).listFiles()) {
			XLog log = null;
			XesXmlGZIPParser parser = new XesXmlGZIPParser();
			if(parser.canParse(inputLogFile)){
				try {
					log = parser.parse(inputLogFile).get(0);
				} catch (Exception e) {
					System.out.println("Unable to load event log: " + inputLogFile.getAbsolutePath());
					e.printStackTrace();
				}
			}


			//Writing event log for discovery
			XLogImpl logSplit = new XLogImpl(log.getAttributes());
			for (XTrace trace : log) {
				XTraceImpl completeEventsTrace = new XTraceImpl(trace.getAttributes());
				for (int j = 0; j < trace.size(); j++) {
					XAttributeLiteralImpl transition = (XAttributeLiteralImpl) trace.get(j).getAttributes().get("lifecycle:transition");
					if (transition.getValue().toLowerCase().equals("complete")) {
						String eventName = XConceptExtension.instance().extractName(trace.get(j)).replace(" ", "_").toLowerCase();
						XEvent ev = trace.get(j);
						ev.getAttributes().put("concept:name", new XAttributeLiteralImpl("concept:name", eventName));
						completeEventsTrace.add(ev);
					}
				}
				logSplit.add(completeEventsTrace);
			}
			FileOutputStream outStream;
			String outputFileName = "discovery_" + inputLogFile.getName();
			try {
				//TODO: Check for export differences between the generation methods
				outStream = new FileOutputStream(outputPath + outputFileName.replace(".gz", ""));
				new XesXmlSerializer().serialize(logSplit, outStream);
				outStream.flush();
				outStream.close();
				System.out.println("Log created: " + outputPath + outputFileName.replace(".gz", "") + " (" + logSplit.size() + " traces)");
			} catch (IOException e) {
				System.out.println("Unable to create log: " + outputFileName);
			}



			//Writing event log for conformance
			logSplit = new XLogImpl(log.getAttributes());
			for (XTrace trace : log) {
				XTraceImpl completeEventsTrace = new XTraceImpl(trace.getAttributes());
				for (int j = 0; j < trace.size(); j++) {
					XAttributeLiteralImpl transition = (XAttributeLiteralImpl) trace.get(j).getAttributes().get("lifecycle:transition");
					if (transition.getValue().toLowerCase().equals("complete")) {
						String eventName = XConceptExtension.instance().extractName(trace.get(j)).replace(" ", "_").toLowerCase();
						XEvent ev = trace.get(j);
						ev.getAttributes().put("concept:name", new XAttributeLiteralImpl("concept:name", eventName));
						completeEventsTrace.add(ev);
					}
				}

				//Adding event "begin"
				XFactory nxFactory = XFactoryRegistry.instance().currentDefault();
				XEvent le = nxFactory.createEvent();
				XConceptExtension nconcept = XConceptExtension.instance();
				nconcept.assignName(le, "begin");
				XLifecycleExtension nlc = XLifecycleExtension.instance();
				nlc.assignTransition(le, "complete");
				XTimeExtension ntimeExtension = XTimeExtension.instance();
				ntimeExtension.assignTimestamp(le, ntimeExtension.extractTimestamp(completeEventsTrace.get(0)).getTime() - 1);
				completeEventsTrace.add(0, le);

				//Adding event "complete"
				nxFactory = XFactoryRegistry.instance().currentDefault();
				le = nxFactory.createEvent();
				nconcept = XConceptExtension.instance();
				nconcept.assignName(le, "complete");
				nlc = XLifecycleExtension.instance();
				nlc.assignTransition(le, "complete");
				ntimeExtension = XTimeExtension.instance();
				ntimeExtension.assignTimestamp(le, ntimeExtension.extractTimestamp(completeEventsTrace.get(completeEventsTrace.size() - 1)).getTime() + 1);
				completeEventsTrace.add(le);

				logSplit.add(completeEventsTrace);
			}

			outputFileName = "conformance_" + inputLogFile.getName();
			try {
				//TODO: Check for export differences between the generation methods
				outStream = new FileOutputStream(outputPath + outputFileName.replace(".gz", ""));
				new XesXmlSerializer().serialize(logSplit, outStream);
				outStream.flush();
				outStream.close();
				System.out.println("Log created: " + outputPath + outputFileName.replace(".gz", "") + " (" + logSplit.size() + " traces)");
			} catch (IOException e) {
				System.out.println("Unable to create log: " + outputFileName);
			}
		}
	}
}
