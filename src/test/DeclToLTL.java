package test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class DeclToLTL {

	public static void main(String[] args) {
//		String inputModelPath = "BPI_2012_test/models/split0_selected_decl_model_w_probabilities.txt";
//		String outputModelPath= "BPI_2012_test/models/split0_ProbDec_model.txt";
		
//		String inputModelPath = "RIO_test/models/part1_selected_constraints_w_probabilities.txt";
//		String outputModelPath= "RIO_test/models/part1_ProbDec_model.txt";
		
		String inputModelPath = "sw_test/models/part1_selected_constraints_w_probabilities_3.txt";
		String outputModelPath= "sw_test/models/part1_selected_constraints_probDecl_3.txt";
		
		try {
			Scanner s = new Scanner(new File(inputModelPath));
			BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputModelPath));
			while (s.hasNextLine()){
				String[] splitLine = s.nextLine().split(";");
				if (splitLine.length==2) {
					String template = splitLine[0].substring(0, splitLine[0].indexOf("["));
					String generalFormula = getFormulaByTemplate(DeclareTemplate.valueOf(template.replace(" ", "_")));
					
					String[] activities = splitLine[0].substring(splitLine[0].indexOf("[")+1, splitLine[0].indexOf("]")).split(", ");
					
					String formula = generalFormula.replace("\"A\"", activities[0]);
					if (activities.length > 1) {
						formula = formula.replace("\"B\"", activities[1]);
					}
					System.out.println(generalFormula);
					System.out.println(formula);
					
					String outLine = formula + ";" + splitLine[1] + "\n";
					System.out.println(outLine);
					writer.write(outLine);
				}
			}
			writer.flush();
			writer.close();
		} catch (FileNotFoundException e) {
			System.out.println("Model file not found: " + inputModelPath);
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Cannot write output model: " + outputModelPath);
			e.printStackTrace();
		}
		
	}
	
	public static String getFormulaByTemplate(DeclareTemplate template){
		String formula = "";
		switch(template){
		case Absence:
			formula = "!( <> ( \"A\" ) )";
			break;
		case Absence2 :
			formula = "! ( <> ( ( \"A\" /\\ X(<>(\"A\")) ) ) )";
			break;
		case Absence3 :
			formula = "! ( <> ( ( \"A\" /\\  X ( <> ((\"A\" /\\  X ( <> ( \"A\" ) )) ) ) ) ))";
			break;
		case Alternate_Precedence :
			formula = "(((( !(\"B\") U \"A\") \\/ []( !(\"B\"))) /\\ []((\"B\" ->( (!(X(\"A\")) /\\ !(X(!(\"A\"))) ) \\/ X((( !(\"B\") U \"A\") \\/ []( !(\"B\")))))))) /\\ !(\"B\"))";
			break;
		case Alternate_Response :
			formula = "( []( ( \"A\" -> X(( (! ( \"A\" )) U \"B\" ) )) ) )";
			break;
		case Alternate_Succession :
			formula = "( []((\"A\" -> X(( !(\"A\") U \"B\")))) /\\ (((( !(\"B\") U \"A\") \\/ []( !(\"B\"))) /\\ []((\"B\" ->( (!(X(\"A\")) /\\ !(X(!(\"A\"))) ) \\/ X((( !(\"B\") U \"A\") \\/ []( !(\"B\")))))))) /\\ !(\"B\")))";
			break;
		case Chain_Precedence :
			formula = "[]( ( X( \"B\" ) -> \"A\") )/\\ ! (\"B\" )";
			break;
		case Chain_Response :
			formula = "[] ( ( \"A\" -> X( \"B\" ) ) )";
			break;
		case Chain_Succession :
			formula = "([]( ( \"A\" -> X( \"B\" ) ) )) /\\ ([]( ( X( \"B\" ) ->  \"A\") ) /\\ ! (\"B\" ))";
			break;
		case Choice :
			formula = "(  <> ( \"A\" ) \\/ <>( \"B\" )  )";
			break;
		case CoExistence :
			formula = "( ( <>(\"A\") -> <>( \"B\" ) ) /\\ ( <>(\"B\") -> <>( \"A\" ) )  )";
			break;
		case Exactly1 :
			formula = "(  <> (\"A\") /\\ ! ( <> ( ( \"A\" /\\ X(<>(\"A\")) ) ) ) )";
			break;
		case Exactly2 :
			formula = "( <> (\"A\" /\\ (\"A\" -> (X(<>(\"A\"))))) /\\  ! ( <>( \"A\" /\\ (\"A\" -> X( <>( \"A\" /\\ (\"A\" -> X ( <> ( \"A\" ) ))) ) ) ) ) )";
			break;
		case Exclusive_Choice :
			formula = "(  ( <>( \"A\" ) \\/ <>( \"B\" )  )  /\\ !( (  <>( \"A\" ) /\\ <>( \"B\" ) ) ) )";
			break;
		case Existence :
			formula = "( <> ( \"A\" ) )";
			break;
		case Existence2 :
			formula = "<> ( ( \"A\" /\\ X(<>(\"A\")) ) )";
			break;
		case Existence3 :
			formula = "<>( \"A\" /\\ X(  <>( \"A\" /\\ X( <> \"A\" )) ))";
			break;
		case Init :
			 formula = "( \"A\" )";
			break;
		case Not_Chain_Succession :
			 formula = "[]( ( \"A\" -> !(X( \"B\" ) ) ))";
			break;
		case Not_CoExistence :
			formula = "(<>(\"A\")) -> (!(<>( \"B\" )))";
			break;
		case Not_Succession :
			formula = "[]( ( \"A\" -> !(<>( \"B\" ) ) ))";
			break;
		case Precedence :
			formula = "( ! (\"B\" ) U \"A\" ) \\/ ([](!(\"B\"))) /\\ ! (\"B\" )";
			break;
		case Responded_Existence :
			formula = "(( ( <>( \"A\" ) -> (<>( \"B\" ) )) ))";
			break;
		case Response :
			formula = "( []( ( \"A\" -> <>( \"B\" ) ) ))";
			break;
		case Succession :
			formula = "(( []( ( \"A\" -> <>( \"B\" ) ) ))) /\\ (( ! (\"B\" ) U \"A\" ) \\/ ([](!(\"B\"))) /\\ ! (\"B\" ))";
			break;		
		
		}
		return formula;
	}
	
	public enum DeclareTemplate {
		Absence, Absence2, Absence3, Alternate_Precedence, Alternate_Response, Alternate_Succession, 
		Chain_Precedence, Chain_Response, Chain_Succession, Choice, CoExistence, 
		Exactly1, Exactly2, Exclusive_Choice, Existence, Existence2, Existence3, 
		Init, Not_Chain_Succession, Not_CoExistence, Not_Succession,
		Precedence, Response, Responded_Existence,
		Succession
	}
}
