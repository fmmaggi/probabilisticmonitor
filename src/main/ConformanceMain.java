package main;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.in.XesXmlGZIPParser;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.ltl2automaton.plugins.automaton.Automaton;
import org.processmining.ltl2automaton.plugins.automaton.State;
import org.processmining.ltl2automaton.plugins.automaton.Transition;
import org.processmining.ltl2automaton.plugins.formula.DefaultParser;
import org.processmining.ltl2automaton.plugins.formula.Formula;
import org.processmining.ltl2automaton.plugins.formula.conjunction.ConjunctionFactory;
import org.processmining.ltl2automaton.plugins.formula.conjunction.ConjunctionTreeLeaf;
import org.processmining.ltl2automaton.plugins.formula.conjunction.ConjunctionTreeNode;
import org.processmining.ltl2automaton.plugins.formula.conjunction.DefaultTreeFactory;
import org.processmining.ltl2automaton.plugins.formula.conjunction.GroupedTreeConjunction;
import org.processmining.ltl2automaton.plugins.formula.conjunction.TreeFactory;
import org.processmining.ltl2automaton.plugins.ltl.SyntaxParserException;
import org.processmining.onlinedeclareanalyzer.lpsolver.LpSolverUtil;
import org.processmining.onlinedeclareanalyzer.lpsolver.ast.BooleanExpression;
import org.processmining.onlinedeclareanalyzer.lpsolver.lexer.Lexer;
import org.processmining.onlinedeclareanalyzer.lpsolver.parser.RecursiveDescentParser;
import org.processmining.plugins.declareminer.ExecutableAutomaton;
import org.processmining.plugins.declareminer.PossibleNodes;

import declare.DeclareConstraint;
import declare.DeclareUtils;

public class ConformanceMain {
	private static Map<String, String> activityToEncoding =  new HashMap<String, String>();
	private static int consistentCount = 0;
	private static int inconsistentCount = 0;

	public static void main(String[] args){
		//Handling cmd arguments
		CommandLine cmd = handleArgs(args);
		String inputModelPath = cmd.getOptionValue("model");
		String inputLogPath = cmd.getOptionValue("log");
		boolean isDeclare = cmd.hasOption("declare");

//		String inputModelPath = "RIO_test/models/part1_ProbDec_model.txt";
//		String inputLogPath = "RIO_test/processed_log/conformance_RIO10k_part1.xes";
		
//		String inputModelPath = "generated_test/model_probDecl.txt";
//		String inputLogPath = "generated_test/processed_log/conformance_negative100.xes";
		
//		String inputModelPath = "sw_test/models/part1_selected_constraints_probDecl_3.txt";
//		String inputLogPath = "sw_test/processed_log_incremental/conformance_sw10k_parts1-10.xes";
		
//		String inputModelPath = "BPI_2018/2_EMD_performance/ful_scenario_9.decl";
//		String inputLogPath = "BPI_2018/0_event_logs/BPI Challenge 2018 - year 2017.xes";
//		boolean isDeclare = true;
		
		long taskStartTime = System.currentTimeMillis();
		System.out.println("EMD conformance checking started at: " + taskStartTime);

		System.out.println("############################");
		System.out.println("Reading the model");
		System.out.println("############################");
		List<String[]> inputModel = readInputModel(inputModelPath, isDeclare);

		System.out.println("############################");
		System.out.println("Creating scenario bitsets");
		System.out.println("############################");
		List<BitSet> scenarios = new ArrayList<BitSet>();
		if (inputModel.size() <= 8) {
			for (int i = 0; i < Math.pow(2, inputModel.size()); i++) {
				BitSet scenario = BitSet.valueOf(new byte[]{new Integer(i).byteValue()});
				scenarios.add(scenario);
			}
		} else {
			for (int i = 0; i < Math.pow(2, inputModel.size()); i++) {
				BitSet scenario = BitSet.valueOf(new long[]{new Integer(i).longValue()});
				scenarios.add(scenario);
			}
		}

		System.out.println("===================================");
		System.out.println("Creating scenario inequality strings");
		System.out.println("===================================");
		List<String> scenarioInequalities = createScenarioInequalities(scenarios, inputModel);
		System.out.println("===================================");
		System.out.println("ConformanceMain: Creating relocation inequality strings based on model");
		System.out.println("===================================");
		List<String> modelRelocationInequalities = createModelRelocationInequalities(scenarioInequalities, scenarios.size());

		System.out.println("===================================");
		System.out.println("Creating relocation inequality strings based on event log");
		System.out.println("===================================");
		List<String> logRelocationInequalities = createLogRelocationInequalities(scenarios, inputModel, inputLogPath, isDeclare);

		System.out.println("===================================");
		System.out.println("Creating relocation cost inequality strings");
		System.out.println("===================================");
		List<String> realocationCostInequalities = createRelocationCostInequalities(scenarios, inputModel.size());

		System.out.println("===================================");
		System.out.println("Calculating the conformance measure");
		System.out.println("===================================");
		double emdConformance = calculateEmdConformance(modelRelocationInequalities, logRelocationInequalities, realocationCostInequalities);

		System.out.println("\n============ Results ============");
		System.out.println("Model: " + inputModelPath);
		System.out.println("Log: " + inputLogPath);
		System.out.println("Consistent scenarios: " + consistentCount + "; Inconsistent scenarios: " + inconsistentCount);
		System.out.println("Conformance: " + emdConformance);
		System.out.println("\nEMD conformance checking ended at: " + taskStartTime + " - total time: " + (System.currentTimeMillis() - taskStartTime));

	}



	private static double calculateEmdConformance(List<String> modelRelocationInequalities, List<String> logRelocationInequalities, List<String> realocationCostInequalities) {
		StringBuilder inequalitySb = new StringBuilder("");
		inequalitySb.append(String.join(" && ", modelRelocationInequalities));
		inequalitySb.append(" && ");
		inequalitySb.append(String.join(" && ", logRelocationInequalities));
		inequalitySb.append(" && ");
		inequalitySb.append(String.join(" && ", realocationCostInequalities));
		String exP = inequalitySb.toString();

		Lexer lexer = new Lexer(new ByteArrayInputStream(exP.getBytes()));
		RecursiveDescentParser parser = new RecursiveDescentParser(lexer);
		BooleanExpression ast = parser.build();
		ArrayList<ArrayList<BooleanExpression>> problemSets1 = ast.interpret();

		HashMap<String, Double> resultMap = null;
		for (ArrayList<BooleanExpression> problemSet : problemSets1) {
			resultMap = LpSolverUtil.getResults(problemSet, "cost");
		}

		/*
		 * Using the average of the result interval, instead of just the minimum, because that gives more sensible results in practice
		 * For example if I have: two sets of constraint probabilities (1.0, 0.5, 0.5 and 0.9, 0.5, 0.5) and a log where all of these constraints are satisfied
		 * then both sets would get the same EMD distance of 0.83 when using only the minimum value
		 * but when using the average of the interval then the first gets 0.665 while the second gets 0.6
		 */
		
		//return 1 - resultMap.get("min");
		return 1 - ((resultMap.get("min") + resultMap.get("max"))/2);
	}


	private static List<String> createLogRelocationInequalities(List<BitSet> scenarios, List<String[]> inputModel, String inputLogPath, boolean isDeclare) {
		List<String> relocationLogStrings = new ArrayList<String>();
		StringBuilder inequalitySb;

		//Loading the log file
		XLog log = null;
		File inputLogFile = new File(inputLogPath);
		
		if (inputLogFile.getName().toLowerCase().endsWith(".xes")) {
			XesXmlParser parser = new XesXmlParser();
			if(parser.canParse(inputLogFile)){
				try {
					log = parser.parse(inputLogFile).get(0);
				} catch (Exception e) {
					System.out.println("Unable to load event log: " + inputLogPath);
				}
			}
		} else if (inputLogFile.getName().toLowerCase().endsWith(".xes.gz")) {
			XesXmlGZIPParser parser = new XesXmlGZIPParser();
			if(parser.canParse(inputLogFile)){
				try {
					log = parser.parse(inputLogFile).get(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		} else {
			System.out.println("Unsupported log file extension");
		}
		
		

		//Creating automatons for each constraint
		List<ExecutableAutomaton> automatons = new ArrayList<ExecutableAutomaton>();
		TreeFactory<ConjunctionTreeNode, ConjunctionTreeLeaf> treeFactory = DefaultTreeFactory.getInstance();
		ConjunctionFactory<? extends GroupedTreeConjunction> conjunctionFactory = GroupedTreeConjunction.getFactory(treeFactory);
		for (String[] modelElement : inputModel) {
			//Parsing the constraint
			List<Formula> formulaeParsed = new ArrayList<Formula>();
			try {
				formulaeParsed.add(new DefaultParser(modelElement[0]).parse());
			} catch (SyntaxParserException e) {
				System.out.println("Unable to parse formula: " + modelElement[0]);
				e.printStackTrace();
			}

			//Creating the automaton
			GroupedTreeConjunction conjunction = conjunctionFactory.instance(formulaeParsed);
			Automaton aut = conjunction.getAutomaton().op.reduce();
			ExecutableAutomaton execAut = new ExecutableAutomaton(aut);
			automatons.add(execAut);
		}

		//Creating a map to count the number of traces belonging to each scenario
		Map<BitSet, Integer> traceCountMap = new HashMap<BitSet, Integer>();
		for (BitSet scenario : scenarios) {
			traceCountMap.put(scenario, 0);
		}

		//Map for tracking automaton states
		Map<ExecutableAutomaton, String> truthValues = new HashMap<ExecutableAutomaton, String>(automatons.size());

		//Finding the scenario of each trace
		BitSet traceScenario = new BitSet(scenarios.size());
		for (XTrace trace : log) {
			//Reset automatons, trace scenario and truth values
			traceScenario.clear();
			for (ExecutableAutomaton executableAutomaton : automatons) {
				executableAutomaton.ini();
				truthValues.put(executableAutomaton, "init");
			}

			//Execute each event on each constraint automaton
			for (XEvent xevent : trace) {
				String event = XConceptExtension.instance().extractName(xevent);
				if (isDeclare) {
					event = activityToEncoding.getOrDefault(event, "actx");
				}
				
				for (ExecutableAutomaton executableAutomaton : automatons) {
					PossibleNodes current = executableAutomaton.currentState();
					boolean violated = true;
					String truthValue;
					if(truthValues.get(executableAutomaton).equals("sat") || truthValues.get(executableAutomaton).equals("viol")){
						continue;
					}
					if(current!=null&& !(current.get(0)==null)){
						for (Transition out : current.output()) {
							if (out.parses(event)) {
								violated = false;
								break;
							}
						}
					}

					if (!violated){
						if(!event.equals("begin")){
							ExecutableAutomaton exec = executableAutomaton;
							exec.next(event);
						}
						current = executableAutomaton.currentState();
						if (current.isAccepting()) {
							truthValue = "poss.sat";
							for (State state : current) {
								if (state.isAccepting()) {
									for (Transition t : state.getOutput()) {
										if (t.isAll() && t.getTarget().equals(state)) {
											truthValue = "sat";
										}
									}
								}
							}
						} else {
							truthValue = "poss.viol";
						}

					} else {
						truthValue = "viol";
					}
					truthValues.put(executableAutomaton, truthValue);
				}
			}
			
			//Setting temporary states to final at the end of the trace
			for (ExecutableAutomaton executableAutomaton : automatons) {
				if (truthValues.get(executableAutomaton).equals("poss.sat")) {
					truthValues.put(executableAutomaton, "sat");
				} else if(truthValues.get(executableAutomaton).equals("poss.viol")) {
					truthValues.put(executableAutomaton, "viol");
				}
			}

			System.out.println("\nTrace: " + XConceptExtension.instance().extractName(trace));
			for (int i = 0; i < automatons.size(); i++) {
				System.out.println("State of constraint " + i + ": " + truthValues.get(automatons.get(i)));

				if (truthValues.get(automatons.get(i)).equals("sat")) {
					traceScenario.set(i);
				}
			}
			traceCountMap.put(traceScenario, traceCountMap.get(traceScenario) + 1);
		}

		System.out.println();
		for (int i = 0; i < scenarios.size(); i++) {
			inequalitySb = new StringBuilder("((");
			for (int j = 0; j < scenarios.size(); j++) {
				inequalitySb.append("r").append(Integer.toString(i)).append("c").append(Integer.toString(j)).append(" + ");
			}
			inequalitySb.setLength(inequalitySb.length()-3);
			inequalitySb.append(") == ");
			inequalitySb.append(traceCountMap.get(scenarios.get(i))*1.0 / log.size()).append(")");
			relocationLogStrings.add(inequalitySb.toString());
			System.out.println("Relocation for r" + i + ": " + inequalitySb.toString());
		}


		return relocationLogStrings;
	}


	//Creating relocation inequality strings based on model
	private static List<String> createModelRelocationInequalities(List<String> scenarioInequalityStrings, int numberOfScenarios) {
		List<String> relocationModelStrings = new ArrayList<String>();
		StringBuilder inequalitySb;

		//Building the expression for lpsolve
		String exP = String.join(" && ", scenarioInequalityStrings);
		Lexer lexer = new Lexer(new ByteArrayInputStream(exP.getBytes()));
		RecursiveDescentParser parser = new RecursiveDescentParser(lexer);
		BooleanExpression ast = parser.build();
		ArrayList<ArrayList<BooleanExpression>> problemSets1 = ast.interpret();

		//Solving the expression for each scenario
		for (ArrayList<BooleanExpression> problemSet : problemSets1) {
			for (int i = 0; i < numberOfScenarios; i++) {
				HashMap<String, Double> resultMap = LpSolverUtil.getResults(problemSet, "x"+Integer.toString(i));
				double min = resultMap.get("min");
				double max = resultMap.get("max");
				System.out.println("Result for " + "x" +Integer.toString(i) + ": ["  + min + "," + max + "]");

				//Variables for model relocation inequality
				inequalitySb = new StringBuilder("((");
				for (int j = 0; j < numberOfScenarios; j++) {
					inequalitySb.append("r").append(Integer.toString(j)).append("c").append(Integer.toString(i)).append(" + ");
				}
				inequalitySb.setLength(inequalitySb.length()-3);

				if (min==max) {
					//If there is no interval then variables sum up to scenario probability in the model
					inequalitySb.append(") == ").append(Double.toString(min)).append(")");
					relocationModelStrings.add(inequalitySb.toString());
					//System.out.println("Relocation for c" + i + ": " + inequalitySb.toString());
				} else {
					String leftHandSide = inequalitySb.toString();
							
					inequalitySb.append(") >= ").append(Double.toString(min)).append(")");
					relocationModelStrings.add(inequalitySb.toString());
					//System.out.println("Lower bound for c" + i + ": " + inequalitySb.toString());
					
					inequalitySb = new StringBuilder(leftHandSide);
					inequalitySb.append(") <= ").append(Double.toString(max)).append(")");
					relocationModelStrings.add(inequalitySb.toString());
					//System.out.println("Upper bound for c" + i + ": " + inequalitySb.toString());
				}
			}
		}

		return relocationModelStrings;
	}


	//Creating scenario inequality strings
	private static List<String> createScenarioInequalities(List<BitSet> scenarios, List<String[]> inputModel) {
		List<String> scenarioInequalities = new ArrayList<String>();
		StringBuilder inequalitySb;

		//Inequality for all scenarios (a completed trace will always fit exactly one scenario)
		inequalitySb = new StringBuilder("((");
		for (int i = 0; i < scenarios.size(); i++) {
			inequalitySb.append("x").append(Integer.toString(i)).append("+");
		}
		inequalitySb.setLength(inequalitySb.length()-1);
		inequalitySb.append(") == 1.0)");
		scenarioInequalities.add(inequalitySb.toString());
		System.out.println("All scenarios: " + inequalitySb.toString());

		//Inequalities for consistent/inconsistent scenarios (equals to or greater than 0.0 respectively)
		TreeFactory<ConjunctionTreeNode, ConjunctionTreeLeaf> treeFactory = DefaultTreeFactory.getInstance();
		ConjunctionFactory<? extends GroupedTreeConjunction> conjunctionFactory = GroupedTreeConjunction.getFactory(treeFactory);
		for (int i = 0; i < scenarios.size(); i++) {
			StringJoiner formulaJoiner = new StringJoiner(" /\\ ");

			//Adding each constraint to the formula according to the corresponding true/false value in the scenario
			for (int j = 0; j < inputModel.size(); j++) {
				if (scenarios.get(i).get(j)) {
					formulaJoiner.add("(" + inputModel.get(j)[0] + ")");
				} else {
					formulaJoiner.add("!(" + inputModel.get(j)[0] + ")");
				}
			}

			//Parsing the formula
			List<Formula> formulaeParsed = new ArrayList<Formula>();
			try {
				formulaeParsed.add(new DefaultParser(formulaJoiner.toString()).parse());
			} catch (SyntaxParserException e) {
				System.out.println("Unable to parse formula: " + formulaJoiner.toString());
				e.printStackTrace();
			}

			//Detecting consistent/inconsistent scenarios by creating and checking automatons of each scenario
			GroupedTreeConjunction conjunction = conjunctionFactory.instance(formulaeParsed);
			Automaton aut = conjunction.getAutomaton().op.reduce();
			if(aut.op.isEmpty()){
				//If the automata is empty then the corresponding scenario is inconsistent (cannot logically happen)
				inequalitySb = new StringBuilder("((");
				inequalitySb.append("x").append(Integer.toString(i)).append(") == 0.0)");
				scenarioInequalities.add(inequalitySb.toString());
				inconsistentCount++;
				System.out.println("Inconsistent: " + inequalitySb.toString());
			} else {
				//If the automata is not empty then the corresponding scenario is consistent (can logically happen)
				inequalitySb = new StringBuilder("((");
				inequalitySb.append("x").append(Integer.toString(i)).append(") >= 0.0)");
				scenarioInequalities.add(inequalitySb.toString());
				consistentCount++;
				System.out.println("Consistent: " + inequalitySb.toString());
			}
		}


		//Constraint specific inequalities (constraint probability must match the sum of corresponding scenarios)
		for (int i = 0; i < inputModel.size(); i++) {
			inequalitySb = new StringBuilder("((");
			for (int j = 0; j < scenarios.size(); j++) {
				if (scenarios.get(j).get(i)) {
					inequalitySb.append("x").append(Integer.toString(j)).append("+");
				}
			}
			inequalitySb.setLength(inequalitySb.length()-1);
			inequalitySb.append(") == ").append(inputModel.get(i)[1]).append(")");
			scenarioInequalities.add(inequalitySb.toString());
			System.out.println("Constraint " + i + ": " + inequalitySb.toString());
		}

		return scenarioInequalities;
	}


	//Creating relocation cost inequality strings
	public static List<String> createRelocationCostInequalities(List<BitSet> scenarios, int numberOfConstraints) {
		List<String> costInequalities = new ArrayList<String>();
		//costInequalities.add("((cost) >= 0)");
		//costInequalities.add("((cost) <= 1)");
		for (int row = 0; row < scenarios.size(); row++) {
			StringBuilder inequalitySb  = new StringBuilder("((");
			for (int col = 0; col < scenarios.size(); col++) {
				if (row == col) {
					inequalitySb.append("0");

					System.out.println("Row " + row + ": " + scenarios.get(row).toString() + "; Col " + col + ": " + scenarios.get(col).toString());
					System.out.println("Distance: " + 0 + "\n");
				} else {
					BitSet distanceBitSet = (BitSet)scenarios.get(row).clone();
					distanceBitSet.xor(scenarios.get(col));
					double distance = distanceBitSet.cardinality()*1.0 / numberOfConstraints;
					inequalitySb.append(Double.toString(distance)).append("");

					System.out.println("Row " + row + ": " + scenarios.get(row).toString() + "; Col " + col + ": " + scenarios.get(col).toString());
					System.out.println("Distance: " + distance + "\n");
				}
				inequalitySb.append("r").append(Integer.toString(row));
				inequalitySb.append("c").append(Integer.toString(col));
				inequalitySb.append(" + ");
			}

			inequalitySb.setLength(inequalitySb.length()-3);
			inequalitySb.append(") == cost").append(row).append(")");
			
			costInequalities.add(inequalitySb.toString());
		}
		
		StringBuilder inequalitySb  = new StringBuilder("((");
		for (int row = 0; row < scenarios.size(); row++) {
			inequalitySb.append("cost").append(row).append(" + ");
		}
		inequalitySb.setLength(inequalitySb.length()-3);
		inequalitySb.append(") == cost)");
		costInequalities.add(inequalitySb.toString());

		return costInequalities;
	}


	//Reading the model
	public static List<String[]> readInputModel(String inputModelPath, boolean isDeclare) {
		List<String[]> splitLines = new ArrayList<String[]>(); 
		try {
			Scanner s = new Scanner(new File(inputModelPath));
			if (isDeclare) {
				Pattern constraintPattern = Pattern.compile("\\w+(\\[.*\\]) \\|");
				
				while (s.hasNextLine()){
					String[] splitLine = s.nextLine().split(";");
					if (splitLine.length==2) {
						try {
							Double.parseDouble(splitLine[1]);
							
							Matcher constraintMatcher = constraintPattern.matcher(splitLine[0]);
							if (constraintMatcher.find()) {
								DeclareConstraint declareConstraint = DeclareUtils.readConstraintString(splitLine[0]);
								String ltlFormula = DeclareUtils.getGenericLtlFormula(declareConstraint.getTemplate());

								//Replacing activity placeholders in the generic formula with activity encodings based on the model
								if (!activityToEncoding.containsKey(declareConstraint.getActivationActivity())) {
									activityToEncoding.put(declareConstraint.getActivationActivity(), "act" + activityToEncoding.size());
								}
								ltlFormula = ltlFormula.replace("\"A\"", activityToEncoding.get(declareConstraint.getActivationActivity()));
								if (declareConstraint.getTemplate().getIsBinary()) {
									if (!activityToEncoding.containsKey(declareConstraint.getTargetActivity())) {
										activityToEncoding.put(declareConstraint.getTargetActivity(), "act" + activityToEncoding.size());
									}
									ltlFormula = ltlFormula.replace("\"B\"", activityToEncoding.get(declareConstraint.getTargetActivity()));
								}
								splitLine[0] = ltlFormula;
								splitLines.add(splitLine);
							}
						} catch (NumberFormatException  e1) {
							System.out.println("Invalid probability value\n" + e1);
						}
					}
				}
			} else {
				while (s.hasNextLine()){
					String[] splitLine = s.nextLine().split(";");
					if (splitLine.length==2) {
						try {
							Double.parseDouble(splitLine[1]);
							splitLines.add(splitLine);
						} catch (NumberFormatException  e1) {
							System.out.println("Invalid probability value\n" + e1);
						}
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("Model file not found: " + inputModelPath);
			e.printStackTrace();
		}
		return splitLines;
	}


	//Handling cmd arguments
	private static CommandLine handleArgs(String[] args) {
		Options options = new Options();
		Option modelParam = new Option("m", "model", true, "input model path");
		modelParam.setRequired(true);
		options.addOption(modelParam);

		Option logParam = new Option("l", "log", true, "input log path");
		logParam.setRequired(true);
		options.addOption(logParam);
		
		Option declareParam = new Option("declare", false, "used if the model contains Declare constraints instead of LTL formulas");
		declareParam.setRequired(false);
		options.addOption(declareParam);

		CommandLineParser parser = new org.apache.commons.cli.DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd = null;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("java -jar .\\EMDConformance.jar ", options);
			System.exit(1);
		}

		return cmd;
	}
}
