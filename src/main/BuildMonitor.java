package main;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.deckfour.xes.extension.std.XConceptExtension;
import org.deckfour.xes.in.XesXmlGZIPParser;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.processmining.ltl2automaton.plugins.automaton.Automaton;
import org.processmining.ltl2automaton.plugins.automaton.State;
import org.processmining.ltl2automaton.plugins.automaton.Transition;
import org.processmining.ltl2automaton.plugins.formula.DefaultParser;
import org.processmining.ltl2automaton.plugins.formula.Formula;
import org.processmining.ltl2automaton.plugins.formula.conjunction.ConjunctionFactory;
import org.processmining.ltl2automaton.plugins.formula.conjunction.ConjunctionTreeLeaf;
import org.processmining.ltl2automaton.plugins.formula.conjunction.ConjunctionTreeNode;
import org.processmining.ltl2automaton.plugins.formula.conjunction.DefaultTreeFactory;
import org.processmining.ltl2automaton.plugins.formula.conjunction.GroupedTreeConjunction;
import org.processmining.ltl2automaton.plugins.formula.conjunction.TreeFactory;
import org.processmining.ltl2automaton.plugins.ltl.SyntaxParserException;
import org.processmining.onlinedeclareanalyzer.lpsolver.LpSolverUtil;
import org.processmining.onlinedeclareanalyzer.lpsolver.ast.BooleanExpression;
import org.processmining.onlinedeclareanalyzer.lpsolver.lexer.Lexer;
import org.processmining.onlinedeclareanalyzer.lpsolver.parser.RecursiveDescentParser;
import org.processmining.plugins.declareminer.ExecutableAutomaton;
import org.processmining.plugins.declareminer.PossibleNodes;

import declare.DeclareConstraint;
import declare.DeclareUtils;

public class BuildMonitor {
	private static Map<String, String> activityToEncoding =  new HashMap<String, String>();
	private static HashMap<String,Double> probabilistic_formulas = new HashMap<String,Double>();
	private static ArrayList<String> positive_formulas = new ArrayList<String>();
	private static ArrayList<String> positive_labels = new ArrayList<String>();
	private static String[][] formula_combinations;
	private static ArrayList<String> empty_automata = new ArrayList<String>();
	private static ArrayList<String> non_empty_automata = new ArrayList<String>();
	private static ArrayList<ArrayList<BooleanExpression>> problemSets1;


	public static void main(String[] args){

		//Handling cmd arguments
//		CommandLine cmd = handleArgs(args);
//		String inputModelPath = cmd.getOptionValue("model");
//		String inputLogPath = cmd.getOptionValue("log");
//		boolean isDeclare = cmd.hasOption("declare");
//		Integer maxTraces = null;
//		if (cmd.hasOption("maxTraces")) {
//			try {
//				maxTraces = Integer.parseInt(cmd.getOptionValue("maxTraces"));
//			} catch (NumberFormatException e) {
//				System.out.println("Invalid value for parameter maxTraces, defaulting to all traces");
//			}
//		}

//		String inputModelPath = "generated_test/model_probDecl.txt";
//		String inputLogPath = "generated_test/processed_log/conformance_negative50.xes";
//		boolean isDeclare = false;
		
		String inputModelPath = "BPI_2018/2_EMD_performance/viol_scenario_9.decl";
		String inputLogPath = "BPI_2018/0_event_logs/BPI Challenge 2018 - year 2017.xes";
		boolean isDeclare = true;
		Integer maxTraces = 100;
		
		long modelPreprocessingTime;
		LinkedList<Long> traceProcessingTimes = new LinkedList<Long>();
		LinkedList<Long> eventProcessingTimes = new LinkedList<Long>();
		long totalTime;
		
		
		
		
		long monitorStartTime = System.currentTimeMillis();
		System.out.println("ProbDeclare monitoring started at: " + monitorStartTime);

		ArrayList<VisualizationItem> items = getMonitoredItems(inputModelPath, isDeclare);
		modelPreprocessingTime = System.currentTimeMillis() - monitorStartTime;
		
		printItems(items);
		//initializeItems(items);

		//		ArrayList<String> trace = new ArrayList<String>();
		//		trace.add("begin");
		//		trace.add("R");
		//		trace.add("C");
		//		trace.add("complete");
		
		//		ArrayList<String> trace = getTraceEvents(inputLogPath);
		
		
		//Loading the log file
		XLog log = null;
		File inputLogFile = new File(inputLogPath);
		
		if (inputLogFile.getName().toLowerCase().endsWith(".xes")) {
			XesXmlParser parser = new XesXmlParser();
			if(parser.canParse(inputLogFile)){
				try {
					log = parser.parse(inputLogFile).get(0);
				} catch (Exception e) {
					System.out.println("Unable to load event log: " + inputLogPath);
				}
			}
		} else if (inputLogFile.getName().toLowerCase().endsWith(".xes.gz")) {
			XesXmlGZIPParser parser = new XesXmlGZIPParser();
			if(parser.canParse(inputLogFile)){
				try {
					log = parser.parse(inputLogFile).get(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		} else {
			System.out.println("Unsupported log file extension");
		}
		
		
		if (maxTraces == null) {
			maxTraces = log.size();
		}
		
		int counter = 0;
		for (XTrace trace : log) {
			long traceProcessingStart = System.currentTimeMillis();
			
			
			initializeItems(items);
			
			System.out.println("\n\n\n====================================================");
			System.out.println("Trace: " + XConceptExtension.instance().extractName(trace));
			System.out.println("====================================================");
			
			for (XEvent xevent : trace) {
				long eventProcessingStart = System.currentTimeMillis();
				
				String event = XConceptExtension.instance().extractName(xevent);
				
				ArrayList<VisualizationItem> possSat = new ArrayList<VisualizationItem>(); 
				ArrayList<VisualizationItem> possViol = new ArrayList<VisualizationItem>(); 
				ArrayList<VisualizationItem> sat = new ArrayList<VisualizationItem>();
				ArrayList<VisualizationItem> viol = new ArrayList<VisualizationItem>();
				System.out.println("\n@@@@@@@@@@@@@");
				System.out.println("Event: "+event);
				System.out.println("@@@@@@@@@@@@@");
				for(VisualizationItem item : items){
					if(event.equals("complete")){
						String truthValue = item.getTruthValue();
						if(truthValue.equals("sat")){
							System.out.println("Scenario: "+item.getVariable()+" sat");
							sat.add(item);		
						}
						if(truthValue.equals("poss.sat")){
							System.out.println("Scenario: "+item.getVariable()+" sat");
							sat.add(item);		
						}
						if(truthValue.equals("viol")){
							System.out.println("Scenario: "+item.getVariable()+" viol");
							viol.add(item);		
						}
						if(truthValue.equals("poss.viol")){
							System.out.println("Scenario: "+item.getVariable()+" viol");
							viol.add(item);		
						}
					}else{
						if (isDeclare) {
							event = activityToEncoding.getOrDefault(event, "actx");
						}
						String truthValue = fireEventOnItem(event, item);
						System.out.println("Scenario: "+item.getVariable()+" "+truthValue);
						if(truthValue.equals("sat")){
							sat.add(item);		
						}
						if(truthValue.equals("poss.sat")){
							possSat.add(item);
						}
						if(truthValue.equals("viol")){
							viol.add(item);
						}
						if(truthValue.equals("poss.viol")){
							possViol.add(item);
						}
					}
				}
				Map<String,Boundaries> currentBoundaries = getGlobalTruthValues(inputModelPath, possSat, possViol, sat, viol, isDeclare);

				printCurrentBoundaries(currentBoundaries);
				
				eventProcessingTimes.add(System.currentTimeMillis() - eventProcessingStart);
				
			}
			
			traceProcessingTimes.add(System.currentTimeMillis() - traceProcessingStart);
			counter++;
			if (counter>=maxTraces) {
				break;
			}
		}
		
		totalTime = System.currentTimeMillis() - monitorStartTime;
		
		System.out.println("\nProbDeclare monitoring ended at: " + System.currentTimeMillis());
		
		System.out.println("\nModel preprocessing time: " + modelPreprocessingTime);
		
		LongSummaryStatistics eventStats = eventProcessingTimes.stream()
                .mapToLong((x) -> x)
                .summaryStatistics();
		System.out.println("Event processing times");
		System.out.println("\tMin: " + eventStats.getMin());
		System.out.println("\tAvg: " + eventStats.getAverage());
		System.out.println("\tMax: " + eventStats.getMax());
		
		LongSummaryStatistics traceStats = traceProcessingTimes.stream()
                .mapToLong((x) -> x)
                .summaryStatistics();
		System.out.println("Trace processing times");
		System.out.println("\tMin: " + traceStats.getMin());
		System.out.println("\tAvg: " + traceStats.getAverage());
		System.out.println("\tMax: " + traceStats.getMax());
		
		System.out.println("Total time: " + totalTime);
	}

	/*
	 *  example input:
	 *  
	 *  <>C;0.6
	 *  []B;0.4
	 *  
	 */	
	public static ArrayList<VisualizationItem> getMonitoredItems(String inputFile, boolean isDeclare){
		BufferedReader br = null;
		ArrayList<VisualizationItem> items = new ArrayList<VisualizationItem>();

		try {
			FileReader input = new FileReader(inputFile);
			br = new BufferedReader(input);
			
			if (isDeclare) {
				Pattern constraintPattern = Pattern.compile("\\w+(\\[.*\\]) \\|");
				String line = br.readLine();
				while (line!=null){
					String[] splitLine = line.split(";");
					if (splitLine.length==2) {
						try {
							Double.parseDouble(splitLine[1]);
							
							Matcher constraintMatcher = constraintPattern.matcher(splitLine[0]);
							if (constraintMatcher.find()) {
								DeclareConstraint declareConstraint = DeclareUtils.readConstraintString(splitLine[0]);
								String ltlFormula = DeclareUtils.getGenericLtlFormula(declareConstraint.getTemplate());

								//Replacing activity placeholders in the generic formula with activity encodings based on the model
								if (!activityToEncoding.containsKey(declareConstraint.getActivationActivity())) {
									activityToEncoding.put(declareConstraint.getActivationActivity(), "act" + activityToEncoding.size());
								}
								ltlFormula = ltlFormula.replace("\"A\"", activityToEncoding.get(declareConstraint.getActivationActivity()));
								if (declareConstraint.getTemplate().getIsBinary()) {
									if (!activityToEncoding.containsKey(declareConstraint.getTargetActivity())) {
										activityToEncoding.put(declareConstraint.getTargetActivity(), "act" + activityToEncoding.size());
									}
									ltlFormula = ltlFormula.replace("\"B\"", activityToEncoding.get(declareConstraint.getTargetActivity()));
								}
								
								probabilistic_formulas.put(ltlFormula, new Double(splitLine[1]));
								positive_formulas.add(ltlFormula);
							}
						} catch (NumberFormatException  e1) {
							System.out.println("Invalid probability value\n" + e1);
						}
					}
					line = br.readLine();
				}
			} else {
				String line = br.readLine();
				while (line!=null) {
					if (line.contains(";")) {
						String[] splitLine = line.split(";");
						probabilistic_formulas.put(splitLine[0], new Double(splitLine[1]));
						positive_formulas.add(splitLine[0]);
					}
					line = br.readLine();
				}
				
				//String[] line = br.readLine().split(";");
				//while(!line[1].equals("labels")){
				//	probabilistic_formulas.put(line[0], new Double(line[1]));
				//	positive_formulas.add(line[0]);
				//	line = br.readLine().split(";");
				//}
				//String[] labels = line[0].split(",");
				//for(int i=0; i<labels.length; i++){
				//	positive_labels.add(labels[i]);
				//}
			}
			
			for (int i = 0; i < positive_formulas.size(); i++) {
				positive_labels.add(Integer.toString(i+1));
			}
			
			

			TreeFactory<ConjunctionTreeNode, ConjunctionTreeLeaf> treeFactory = DefaultTreeFactory.getInstance();
			ConjunctionFactory<? extends GroupedTreeConjunction> conjunctionFactory = GroupedTreeConjunction
					.getFactory(treeFactory);
			String[] all_formulas = new String[]{"0","1"};
			DispositionsGenerator formula_combinations_gen = new DispositionsGenerator();
			formula_combinations = formula_combinations_gen.generateDisp(all_formulas, probabilistic_formulas.keySet().size());
			HashMap<String,Automaton> autMap = new HashMap<String,Automaton>();
			HashMap<String,String> labelsMap = new HashMap<String,String>();


			for(int i = 0; i<formula_combinations.length; i++){
				String emptyToadd = "";
				String formula = "";
				String label = "";
				for(int j = 0; j<formula_combinations[0].length; j++){
					emptyToadd= emptyToadd+formula_combinations[i][j];
					if(formula_combinations[i][j].equals("0")){
						formula = formula + "!("+positive_formulas.get(j)+")";
						if(j<formula_combinations[0].length-1){
							formula = formula+"/\\";
						}
						label = label + "!("+positive_labels.get(j)+")";
						if(j<formula_combinations[0].length-1){
							label = label+"/\\";
						}
					}else{
						formula = formula + "("+positive_formulas.get(j)+")";
						if(j<formula_combinations[0].length-1){
							formula = formula+"/\\";
						}
						label = label + ""+positive_labels.get(j)+"";
						if(j<formula_combinations[0].length-1){
							label = label+"/\\";
						}
					}
				}
				System.out.println(formula);
				List<Formula> formulaeParsed = new ArrayList<Formula>();
				try {
					formulaeParsed.add(new DefaultParser(formula).parse());
				} catch (SyntaxParserException e) {
					e.printStackTrace();
				}
				GroupedTreeConjunction conjunction = conjunctionFactory.instance(formulaeParsed);
				Automaton aut = conjunction.getAutomaton().op.reduce();
				if(aut.op.isEmpty()){
					//If the automata is empty then the corresponding scenario is inconsistent (cannot logically happen)
					empty_automata.add(emptyToadd);
					System.out.println("Inconsistent - " + "x"+emptyToadd);
				}else{
					//If the automata is not empty then the corresponding scenario is consistent (can logically happen)
					non_empty_automata.add(emptyToadd);
					System.out.println("Consistent - " + "x"+emptyToadd);
				}
				autMap.put("x"+emptyToadd, aut);
				labelsMap.put("x"+emptyToadd, label);
			}

			//Creating the system of inequalities based on the model

			//Sum of all scenarios where constraint X is not violated equals to probability of constraint X
			String exP = "(";
			for(int l = 0; l<formula_combinations[0].length; l++){
				boolean addBraket = false;
				for(int i = 0; i<formula_combinations.length; i++){
					boolean addVariable = false;
					if(formula_combinations[i][l].equals("1")){
						if(!addBraket){
							exP = exP + "(";
							addBraket = true;
						}
						addVariable = true;
						exP = exP + "x";
						for(int k = 0; k<formula_combinations[0].length; k++){
							exP = exP + formula_combinations[i][k];
						}
					}
					if(addVariable && i<formula_combinations.length-1){
						exP = exP+"+";
					}
					if(addVariable && i==formula_combinations.length-1){
						exP = exP+") == "+probabilistic_formulas.get(positive_formulas.get(l))+")";
						if(l<formula_combinations[0].length-1){
							exP = exP+" && (";
						}
					}
				} 
			}

			//Sum of all scenarios equals to 1 (meaning that a completed trace will always fit exactly one scenario)
			boolean addBraket = false;
			for(int i = 0; i<formula_combinations.length; i++){
				if(!addBraket){
					exP = exP + " && ((";
					addBraket = true;
				}
				exP = exP + "x";
				for(int k = 0; k<formula_combinations[0].length; k++){
					exP = exP + formula_combinations[i][k];
				}
				if(i<formula_combinations.length-1){
					exP = exP+"+";
				}
				if(i==formula_combinations.length-1){
					exP = exP+") == 1.0)";
				}
			}

			//If the scenario is implausible (cannot logically happen) then it has a probability of 0
			for(String empty : empty_automata){
				exP = exP+" && (("+"x"+empty+") == 0.0)";
			}
			//If the scenario is plausible (can logically happen) then it has a probability greater than 0
			for(String nonEmpty : non_empty_automata){
				exP = exP+" && (("+"x"+nonEmpty+") >= 0.0)";
			}

			//Printing the final system of inequalities
			System.out.println(exP);

			//Solving the system of inequalities
			Lexer lexer = new Lexer(new ByteArrayInputStream(exP.getBytes()));
			RecursiveDescentParser parser = new RecursiveDescentParser(lexer);
			BooleanExpression ast = parser.build();
			problemSets1 = ast.interpret();
			for (ArrayList<BooleanExpression> problemSet : problemSets1) {

				for(String variable : labelsMap.keySet()){
					HashMap<String, Double> resultMap = LpSolverUtil.getResults(problemSet, variable);
					System.out.println("Result for min: " + resultMap.get("min"));
					System.out.println("Result for max: " + resultMap.get("max"));
					double maxMap = resultMap.get("max");
					double minMap = resultMap.get("min");
					if(!empty_automata.contains(variable.substring(1))){
						if(!(minMap==0 && maxMap==0)){ 
							VisualizationItem item = new VisualizationItem();
							Automaton autItem = autMap.get(variable);
							ExecutableAutomaton exec = new ExecutableAutomaton(autItem);
							exec.ini();
							item.setAutomaton(autItem);
							item.setLabel(labelsMap.get(variable));
							item.setMinProbability(minMap);
							item.setMaxProbability(maxMap);
							item.setVariable(variable);
							item.setTruthValue("init");
							items.add(item);
						}
					}
				}
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}  catch (NullPointerException e1) {
		}
		return items;
	}

	public static ArrayList<String> getTraceEvents(String inputFile) {		
		SAXBuilder parserSax = new SAXBuilder();
		ArrayList<String> trace = new ArrayList<String>();
		try {
			FileReader input = new FileReader(inputFile);
			BufferedReader br = new BufferedReader(input);
			Document mess = parserSax.build(br);
			Element rootMess = mess.getRootElement();
			List<Element> traceElement = rootMess.getChild("trace").getChildren("event");
			for (Element event : traceElement) {
				List<Element> elements = event.getChildren();
				for (Element element : elements) {
					if (element.getAttribute("key").getValue().equals("concept:name")) {
						trace.add(element.getAttribute("value").getValue());
					}
				}
			}
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return trace;
	}

	public static void printItems(ArrayList<VisualizationItem> items) {
		for(VisualizationItem item : items){
			printItem(item);
		}
	}

	public static void initializeItems(ArrayList<VisualizationItem> items) {
		for(VisualizationItem item : items){
			ExecutableAutomaton exec = new ExecutableAutomaton(item.getAutomaton());
			exec.ini();
			item.setState(exec);
		}
	}

	public static String fireEventOnItem(String event, VisualizationItem item){
		PossibleNodes current = item.getState().currentState();
		boolean violated = true;
		String truthValue;
		if(item.getTruthValue().equals("viol")){
			return "viol";		
		}
		if(item.getTruthValue().equals("sat")){
			return "sat";		
		}
		if(current!=null&& !(current.get(0)==null)){
			for (Transition out : current.output()) {
				if (out.parses(event)) {
					violated = false;
					break;
				}
			}
		}

		if (!violated){
			if(!event.equals("begin")){
				ExecutableAutomaton exec = item.getState();
				exec.next(event);
				item.setState(exec);
			}
			current = item.getState().currentState();
			if (current.isAccepting()) {
				truthValue = "poss.sat";
				for (State state : current) {
					if (state.isAccepting()) {
						for (Transition t : state.getOutput()) {
							if (t.isAll() && t.getTarget().equals(state)) {
								truthValue = "sat";
							}
						}
					}
				}
			} else {
				truthValue = "poss.viol";
			}

		} else {
			truthValue = "viol";
		}
		item.setTruthValue(truthValue);
		return truthValue;
	}

	public static Map<String,Boundaries> getGlobalTruthValues(String inputFile, ArrayList<VisualizationItem> possSat, ArrayList<VisualizationItem> possViol, ArrayList<VisualizationItem> sat, ArrayList<VisualizationItem> viol, boolean isDeclare){
		HashMap<String, Boundaries> output = new HashMap<String, Boundaries>();
		int totalItems = possSat.size() + possViol.size() + viol.size() + sat.size();

		Boundaries iniBounds = new Boundaries();
		iniBounds.setMin(0.);
		iniBounds.setMax(0.);
		output.put("poss.sat", iniBounds);
		output.put("poss.viol", iniBounds);
		output.put("sat", iniBounds);
		output.put("viol", iniBounds);


		if(possSat.size()==totalItems){
			Boundaries boundsPS = new Boundaries();
			boundsPS.setMin(1.);
			boundsPS.setMax(1.);
			output.put("poss.sat", boundsPS);
			Boundaries boundsPV = new Boundaries();
			boundsPV.setMin(0.);
			boundsPV.setMax(0.);
			output.put("poss.viol", boundsPV);
			Boundaries boundsS = new Boundaries();
			boundsS.setMin(0.);
			boundsS.setMax(0.);
			output.put("sat", boundsS);
			Boundaries boundsV = new Boundaries();
			boundsV.setMin(0.);
			boundsV.setMax(0.);
			output.put("viol", boundsV);
		}else if(possViol.size()==totalItems){
			Boundaries boundsPS = new Boundaries();
			boundsPS.setMin(0.);
			boundsPS.setMax(0.);
			output.put("poss.sat", boundsPS);
			Boundaries boundsPV = new Boundaries();
			boundsPV.setMin(1.);
			boundsPV.setMax(1.);
			output.put("poss.viol", boundsPV);
			Boundaries boundsS = new Boundaries();
			boundsS.setMin(0.);
			boundsS.setMax(0.);
			output.put("sat", boundsS);
			Boundaries boundsV = new Boundaries();
			boundsV.setMin(0.);
			boundsV.setMax(0.);
			output.put("viol", boundsV);
		}else if(sat.size()==totalItems){
			Boundaries boundsPS = new Boundaries();
			boundsPS.setMin(0.);
			boundsPS.setMax(0.);
			output.put("poss.sat", boundsPS);
			Boundaries boundsPV = new Boundaries();
			boundsPV.setMin(0.);
			boundsPV.setMax(0.);
			output.put("poss.viol", boundsPV);
			Boundaries boundsS = new Boundaries();
			boundsS.setMin(0.);
			boundsS.setMax(0.);
			output.put("sat", boundsS);
			Boundaries boundsV = new Boundaries();
			boundsV.setMin(1.);
			boundsV.setMax(1.);
			output.put("viol", boundsV);
		}else if(viol.size()==totalItems){
			Boundaries boundsPS = new Boundaries();
			boundsPS.setMin(0.);
			boundsPS.setMax(0.);
			output.put("poss.sat", boundsPS);
			Boundaries boundsPV = new Boundaries();
			boundsPV.setMin(0.);
			boundsPV.setMax(0.);
			output.put("poss.viol", boundsPV);
			Boundaries boundsS = new Boundaries();
			boundsS.setMin(0.);
			boundsS.setMax(0.);
			output.put("sat", boundsS);
			Boundaries boundsV = new Boundaries();
			boundsV.setMin(1.);
			boundsV.setMax(1.);
			output.put("viol", boundsV);
		}

		if(possSat.size()==1){
			Boundaries boundsPS = new Boundaries();
			boundsPS.setMin(possSat.get(0).getMinProbability());
			boundsPS.setMax(possSat.get(0).getMaxProbability());
			output.put("poss.sat", boundsPS);
		}
		if(possViol.size()==1){
			Boundaries boundsPV = new Boundaries();
			boundsPV.setMin(possViol.get(0).getMinProbability());
			boundsPV.setMax(possViol.get(0).getMaxProbability());
			output.put("poss.viol", boundsPV);
		}
		if(sat.size()==1){
			Boundaries boundsS = new Boundaries();
			boundsS.setMin(sat.get(0).getMinProbability());
			boundsS.setMax(sat.get(0).getMaxProbability());
			output.put("sat", boundsS);
		}
		if(viol.size()==1){
			Boundaries boundsV = new Boundaries();
			boundsV.setMin(viol.get(0).getMinProbability());
			boundsV.setMax(viol.get(0).getMaxProbability());
			output.put("viol", boundsV);
		}

		if(possSat.size() < totalItems && possSat.size() > 1){
			String objective = "";
			int counter = 0;
			for(VisualizationItem item : possSat){
				if(counter == possSat.size()-1){
					objective = objective + item.getVariable();
				}else{
					objective = objective + item.getVariable() + "+";	
				}
				counter++;
			}
			output.put("poss.sat", getProbability(objective, isDeclare));
		}
		if(possViol.size() < totalItems && possViol.size() > 1){
			String objective = "";
			int counter = 0;
			for(VisualizationItem item : possViol){
				if(counter == possViol.size()-1){
					objective = objective + item.getVariable();
				}else{
					objective = objective + item.getVariable() + "+";	
				}
				counter++;
			}
			output.put("poss.viol", getProbability(objective, isDeclare));
		}
		if(sat.size() < totalItems && sat.size() > 1){
			String objective = "";
			int counter = 0;
			for(VisualizationItem item : sat){
				if(counter == sat.size()-1){
					objective = objective + item.getVariable();
				}else{
					objective = objective + item.getVariable() + "+";	
				}
				counter++;
			}
			output.put("sat", getProbability(objective, isDeclare));	
		}
		if(viol.size() < totalItems && viol.size() > 1){
			String objective = "";
			int counter = 0;
			for(VisualizationItem item : viol){
				if(counter == viol.size()-1){
					objective = objective + item.getVariable();
				}else{
					objective = objective + item.getVariable() + "+";	
				}
				counter++;
			}
			output.put("viol", getProbability(objective, isDeclare));
		}
		return output;
	}



	private static Boundaries getProbability(String objective, boolean isDeclare){
		Boundaries bounds = new Boundaries();
	
		
		for (ArrayList<BooleanExpression> problemSet : problemSets1) {

			HashMap<String, Double> resultMap = LpSolverUtil.getResults(problemSet, objective);
			System.out.println("Result for min: " + resultMap.get("min"));
			System.out.println("Result for max: " + resultMap.get("max"));
			double maxMap = resultMap.get("max");
			double minMap = resultMap.get("min");
			bounds.setMin(minMap);
			bounds.setMax(maxMap);
		}
	
		return bounds;
	}

	public static void printCurrentBoundaries(Map<String, Boundaries> currentBoundaries) {
		for(String truthValue : currentBoundaries.keySet()){
			System.out.println(truthValue+": ["+currentBoundaries.get(truthValue).getMin()+","+currentBoundaries.get(truthValue).getMax()+"]");
		}	
	}

	private static void printItem(VisualizationItem item){	
		System.out.println(item.getLabel()+" ["+item.getMinProbability()+","+item.getMaxProbability()+"]");
	}

	//Handling cmd arguments
	private static CommandLine handleArgs(String[] args) {
		Options options = new Options();
		Option modelParam = new Option("m", "model", true, "input model path");
		modelParam.setRequired(true);
		options.addOption(modelParam);

		Option logParam = new Option("l", "log", true, "input log file");
		logParam.setRequired(true);
		options.addOption(logParam);
		
		Option declareParam = new Option("declare", false, "used if the model contains Declare constraints instead of LTL formulas");
		declareParam.setRequired(false);
		options.addOption(declareParam);
		
		Option maxTracedsParam = new Option("maxTraces", true, "maximum number of traces to process (default is to process all traces)");
		maxTracedsParam.setRequired(false);
		options.addOption(maxTracedsParam);

		CommandLineParser parser = new org.apache.commons.cli.DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd = null;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("java -jar .\\ProbMonitor.jar ", options);
			System.exit(1);
		}

		return cmd;
	}
}
