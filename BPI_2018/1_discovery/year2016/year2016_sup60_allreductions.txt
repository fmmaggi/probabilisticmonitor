Activities:
1) Control summary - Main - initialize : Exists in 100.00% of traces in the log
2) Geo parcel document - Declared - begin editing : Exists in 100.00% of traces in the log
3) Payment application - Application - begin editing : Exists in 100.00% of traces in the log
4) Payment application - Application - initialize : Exists in 100.00% of traces in the log
5) Geo parcel document - Main - initialize : Exists in 100.00% of traces in the log
6) Payment application - Application - mail income : Exists in 100.00% of traces in the log
7) Payment application - Application - calculate : Exists in 100.00% of traces in the log
8) Control summary - Main - finish editing : Exists in 100.00% of traces in the log
9) Geo parcel document - Declared - create : Exists in 100.00% of traces in the log
10) Control summary - Main - begin editing : Exists in 100.00% of traces in the log
11) Payment application - Application - mail valid : Exists in 100.00% of traces in the log
12) Reference alignment - Main - performed : Exists in 99.99% of traces in the log
13) Reference alignment - Main - initialize : Exists in 99.99% of traces in the log
14) Geo parcel document - Declared - finish editing : Exists in 99.99% of traces in the log
15) Department control parcels - Main - performed : Exists in 99.98% of traces in the log
16) Payment application - Application - finish editing : Exists in 99.97% of traces in the log
17) Payment application - Application - decide : Exists in 99.97% of traces in the log
18) Payment application - Application - finish payment : Exists in 99.97% of traces in the log
19) Payment application - Application - begin payment : Exists in 99.97% of traces in the log
20) Payment application - Application - insert document : Exists in 99.95% of traces in the log
21) Payment application - Application - revoke decision : Exists in 99.32% of traces in the log
22) Payment application - Application - abort payment : Exists in 69.26% of traces in the log
23) Geo parcel document - Declared - calculate : Exists in 54.08% of traces in the log
24) Geo parcel document - Main - save : Exists in 51.21% of traces in the log
25) Reference alignment - Main - save : Exists in 28.87% of traces in the log
26) Control summary - Main - save : Exists in 19.45% of traces in the log
27) Geo parcel document - Main - insert document : Exists in 17.46% of traces in the log
28) Department control parcels - Main - begin editing : Exists in 14.63% of traces in the log
29) Department control parcels - Main - save : Exists in 13.33% of traces in the log
30) Payment application - Main - insert document : Exists in 12.25% of traces in the log
31) Payment application - Change - calculate : Exists in 10.74% of traces in the log
32) Payment application - Change - begin editing : Exists in 10.74% of traces in the log
33) Payment application - Change - initialize : Exists in 10.74% of traces in the log
Constraints:
1) In 100.00% of traces in the log, Payment application - Application - mail valid occurs at least once
2) In 100.00% of traces in the log, Payment application - Application - initialize occurs at least once
3) In 100.00% of traces in the log, Payment application - Application - mail income occurs at least once
4) In 100.00% of traces in the log, Control summary - Main - initialize occurs at least once
5) In 100.00% of traces in the log, Control summary - Main - begin editing occurs at least once
6) In 100.00% of traces in the log, Geo parcel document - Declared - begin editing occurs at least once
7) In 100.00% of traces in the log, Control summary - Main - finish editing occurs at least once
8) In 100.00% of traces in the log, Geo parcel document - Declared - create occurs at least once
9) In 100.00% of traces in the log, Geo parcel document - Main - initialize occurs at least once
10) In 100.00% of traces in the log, Payment application - Application - calculate occurs at least twice
11) In 100.00% of traces in the log, Control summary - Main - begin editing occurs if preceded by Control summary - Main - initialize
12) In 100.00% of traces in the log, If Control summary - Main - begin editing occurs then Control summary - Main - finish editing occurs after Control summary - Main - begin editing
13) In 100.00% of traces in the log, Payment application - Application - mail income occurs at most once
14) In 100.00% of traces in the log, If Payment application - Application - initialize occurs then Payment application - Application - begin editing occurs after Payment application - Application - initialize
15) In 100.00% of traces in the log, Geo parcel document - Main - initialize occurs at most once
16) In 100.00% of traces in the log, Payment application - Application - finish payment occurs at most once
17) In 100.00% of traces in the log, Control summary - Main - finish editing occurs at most once
18) In 100.00% of traces in the log, Payment application - Application - mail valid occurs at most once
19) In 100.00% of traces in the log, Department control parcels - Main - performed occurs at most once
20) In 100.00% of traces in the log, If Geo parcel document - Main - initialize occurs then Geo parcel document - Declared - begin editing occurs after Geo parcel document - Main - initialize
21) In 100.00% of traces in the log, Reference alignment - Main - performed occurs at most once
22) In 100.00% of traces in the log, Control summary - Main - begin editing occurs at most once
23) In 100.00% of traces in the log, Payment application - Application - initialize occurs at most once
24) In 100.00% of traces in the log, Control summary - Main - initialize occurs at most once
25) In 100.00% of traces in the log, Reference alignment - Main - initialize occurs at most once
26) In 100.00% of traces in the log, If Control summary - Main - initialize occurs then Control summary - Main - begin editing occurs after Control summary - Main - initialize
27) In 100.00% of traces in the log, Control summary - Main - finish editing occurs if preceded by Control summary - Main - begin editing
28) In 100.00% of traces in the log, Payment application - Application - calculate occurs if preceded by Payment application - Application - begin editing
29) In 100.00% of traces in the log, Payment application - Application - begin editing occurs if preceded by Payment application - Application - initialize
30) In 100.00% of traces in the log, Geo parcel document - Declared - begin editing occurs if preceded by Geo parcel document - Main - initialize
31) In 99.99% of traces in the log, Reference alignment - Main - initialize occurs at least once
32) In 99.99% of traces in the log, Geo parcel document - Declared - finish editing occurs at least once
33) In 99.99% of traces in the log, Reference alignment - Main - performed occurs at least once
34) In 99.99% of traces in the log, Reference alignment - Main - performed occurs if preceded by Reference alignment - Main - initialize
35) In 99.99% of traces in the log, Payment application - Application - abort payment occurs at most once
36) In 99.99% of traces in the log, If Payment application - Application - mail valid occurs then Geo parcel document - Main - initialize occurs after Payment application - Application - mail valid
37) In 99.99% of traces in the log, If Reference alignment - Main - initialize occurs then Reference alignment - Main - performed occurs after Reference alignment - Main - initialize
38) In 99.99% of traces in the log, Geo parcel document - Main - initialize occurs if preceded by Payment application - Application - mail valid
39) In 99.99% of traces in the log, Geo parcel document - Declared - finish editing occurs if preceded by Geo parcel document - Declared - begin editing
40) In 99.99% of traces in the log, Reference alignment - Main - initialize occurs if preceded by Control summary - Main - finish editing
41) In 99.99% of traces in the log, If Control summary - Main - finish editing occurs then Reference alignment - Main - initialize occurs after Control summary - Main - finish editing
42) In 99.98% of traces in the log, Department control parcels - Main - performed occurs at least once
43) In 99.97% of traces in the log, If Payment application - Application - calculate occurs then Payment application - Application - finish editing occurs after Payment application - Application - calculate
44) In 99.97% of traces in the log, If Payment application - Application - finish editing occurs then Payment application - Application - decide occurs after Payment application - Application - finish editing
45) In 99.97% of traces in the log, Payment application - Application - finish editing occurs if preceded by Payment application - Application - calculate
46) In 99.97% of traces in the log, Payment application - Application - decide occurs if preceded by Payment application - Application - finish editing
47) In 99.97% of traces in the log, Payment application - Application - finish payment occurs at least once
48) In 99.95% of traces in the log, Payment application - Application - insert document occurs at least once
49) In 99.95% of traces in the log, Payment application - Application - finish payment occurs if preceded by Payment application - Application - insert document
50) In 99.86% of traces in the log, If Payment application - Application - insert document occurs then Payment application - Application - finish payment occurs after Payment application - Application - insert document
51) In 99.73% of traces in the log, If Reference alignment - Main - performed occurs then Department control parcels - Main - performed occurs after Reference alignment - Main - performed
52) In 99.73% of traces in the log, Department control parcels - Main - performed occurs if preceded by Reference alignment - Main - performed
53) In 99.73% of traces in the log, Payment application - Application - insert document occurs at most once
54) In 99.64% of traces in the log, Payment application - Application - decide occurs at least twice
55) In 99.55% of traces in the log, Payment application - Application - finish editing occurs at least twice
56) In 99.32% of traces in the log, Payment application - Application - revoke decision occurs at least once
57) In 99.32% of traces in the log, Payment application - Application - begin payment occurs if preceded by Payment application - Application - revoke decision
58) In 99.32% of traces in the log, Payment application - Application - revoke decision occurs if preceded by Payment application - Application - decide
59) In 99.27% of traces in the log, If Payment application - Application - revoke decision occurs then Payment application - Application - calculate occurs after Payment application - Application - revoke decision
60) In 99.16% of traces in the log, If Payment application - Application - begin payment occurs then Payment application - Application - insert document occurs after Payment application - Application - begin payment
61) In 98.42% of traces in the log, Payment application - Application - revoke decision occurs at most once
62) In 97.74% of traces in the log, If Payment application - Application - begin editing occurs then Payment application - Application - revoke decision occurs after Payment application - Application - begin editing
63) In 93.02% of traces in the log, Payment application - Application - initialize occurs if preceded by Department control parcels - Main - performed
64) In 93.02% of traces in the log, If Department control parcels - Main - performed occurs then Payment application - Application - initialize occurs after Department control parcels - Main - performed
65) In 91.62% of traces in the log, Payment application - Application - begin editing occurs at least twice
66) In 89.26% of traces in the log, Payment application - Change - begin editing does not occur
67) In 89.26% of traces in the log, Payment application - Change - calculate does not occur
68) In 89.26% of traces in the log, Payment application - Change - initialize does not occur
69) In 87.75% of traces in the log, Payment application - Main - insert document does not occur
70) In 87.15% of traces in the log, Control summary - Main - initialize occurs if preceded by Geo parcel document - Declared - create
71) In 86.67% of traces in the log, Department control parcels - Main - save does not occur
72) In 85.37% of traces in the log, Department control parcels - Main - begin editing does not occur
73) In 84.03% of traces in the log, If Geo parcel document - Declared - create occurs then Control summary - Main - initialize occurs after Geo parcel document - Declared - create
74) In 82.54% of traces in the log, Geo parcel document - Main - insert document does not occur
75) In 80.55% of traces in the log, Control summary - Main - save does not occur
76) In 77.09% of traces in the log, If Payment application - Application - mail income occurs then Payment application - Application - mail valid occurs after Payment application - Application - mail income
77) In 77.09% of traces in the log, Payment application - Application - mail valid occurs if preceded by Payment application - Application - mail income
78) In 71.95% of traces in the log, Geo parcel document - Declared - create occurs if preceded by Geo parcel document - Declared - finish editing
79) In 71.64% of traces in the log, Geo parcel document - Declared - create occurs at most once
80) In 71.13% of traces in the log, Reference alignment - Main - save does not occur
81) In 69.26% of traces in the log, Payment application - Application - abort payment occurs at least once
82) In 69.26% of traces in the log, Payment application - Application - begin payment occurs at least twice
83) In 69.26% of traces in the log, If Payment application - Application - abort payment occurs then Payment application - Application - begin payment occurs after Payment application - Application - abort payment
84) In 69.26% of traces in the log, Payment application - Application - abort payment occurs if preceded by Payment application - Application - begin payment
85) In 69.26% of traces in the log, If Payment application - Application - decide occurs then Payment application - Application - abort payment occurs after Payment application - Application - decide
86) In 69.23% of traces in the log, Payment application - Application - insert document occurs if preceded by Payment application - Application - abort payment
87) In 69.02% of traces in the log, Geo parcel document - Declared - calculate occurs at most once
88) In 68.83% of traces in the log, Geo parcel document - Main - save occurs at most once
89) In 63.33% of traces in the log, If Control summary - Main - finish editing occurs then Geo parcel document - Declared - finish editing occurs after Control summary - Main - finish editing
90) In 61.52% of traces in the log, If Geo parcel document - Declared - begin editing occurs then Geo parcel document - Declared - create occurs after Geo parcel document - Declared - begin editing
91) In 60.28% of traces in the log, If Geo parcel document - Declared - finish editing occurs then Department control parcels - Main - performed occurs after Geo parcel document - Declared - finish editing
