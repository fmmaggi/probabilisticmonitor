Activities:
1) Control summary - Main - initialize : Exists in 100.00% of traces in the log
2) Geo parcel document - Declared - begin editing : Exists in 100.00% of traces in the log
3) Payment application - Application - begin editing : Exists in 100.00% of traces in the log
4) Payment application - Application - initialize : Exists in 100.00% of traces in the log
5) Geo parcel document - Main - initialize : Exists in 100.00% of traces in the log
6) Payment application - Application - mail income : Exists in 100.00% of traces in the log
7) Payment application - Application - calculate : Exists in 100.00% of traces in the log
8) Control summary - Main - finish editing : Exists in 100.00% of traces in the log
9) Geo parcel document - Declared - create : Exists in 100.00% of traces in the log
10) Control summary - Main - begin editing : Exists in 100.00% of traces in the log
11) Payment application - Application - mail valid : Exists in 100.00% of traces in the log
12) Reference alignment - Main - performed : Exists in 99.99% of traces in the log
13) Reference alignment - Main - initialize : Exists in 99.99% of traces in the log
14) Geo parcel document - Declared - finish editing : Exists in 99.99% of traces in the log
15) Department control parcels - Main - performed : Exists in 99.98% of traces in the log
16) Payment application - Application - finish editing : Exists in 99.97% of traces in the log
17) Payment application - Application - decide : Exists in 99.97% of traces in the log
18) Payment application - Application - finish payment : Exists in 99.97% of traces in the log
19) Payment application - Application - begin payment : Exists in 99.97% of traces in the log
20) Payment application - Application - insert document : Exists in 99.95% of traces in the log
21) Payment application - Application - revoke decision : Exists in 99.32% of traces in the log
22) Payment application - Application - abort payment : Exists in 69.26% of traces in the log
23) Reference alignment - Main - save : Exists in 28.87% of traces in the log
24) Control summary - Main - save : Exists in 19.45% of traces in the log
25) Geo parcel document - Main - insert document : Exists in 17.46% of traces in the log
26) Department control parcels - Main - begin editing : Exists in 14.63% of traces in the log
27) Department control parcels - Main - save : Exists in 13.33% of traces in the log
28) Payment application - Main - insert document : Exists in 12.25% of traces in the log
29) Payment application - Change - calculate : Exists in 10.74% of traces in the log
30) Payment application - Change - begin editing : Exists in 10.74% of traces in the log
31) Payment application - Change - initialize : Exists in 10.74% of traces in the log
Constraints:
1) In 100.00% of traces in the log, Payment application - Application - mail valid occurs at least once
2) In 100.00% of traces in the log, Payment application - Application - initialize occurs at least once
3) In 100.00% of traces in the log, Payment application - Application - mail income occurs at least once
4) In 100.00% of traces in the log, Payment application - Application - initialize occurs at most once
5) In 100.00% of traces in the log, Control summary - Main - initialize occurs at least once
6) In 100.00% of traces in the log, Control summary - Main - begin editing occurs at least once
7) In 100.00% of traces in the log, Geo parcel document - Declared - begin editing occurs at least once
8) In 100.00% of traces in the log, Control summary - Main - finish editing occurs at least once
9) In 100.00% of traces in the log, Geo parcel document - Declared - create occurs at least once
10) In 100.00% of traces in the log, Geo parcel document - Main - initialize occurs at least once
11) In 100.00% of traces in the log, Payment application - Application - calculate occurs at least twice
12) In 100.00% of traces in the log, Control summary - Main - initialize occurs at most once
13) In 100.00% of traces in the log, Reference alignment - Main - initialize occurs at most once
14) In 100.00% of traces in the log, If Control summary - Main - begin editing occurs then Control summary - Main - finish editing occurs after Control summary - Main - begin editing
15) In 100.00% of traces in the log, If Payment application - Application - initialize occurs then Payment application - Application - begin editing occurs after Payment application - Application - initialize
16) In 100.00% of traces in the log, If Geo parcel document - Main - initialize occurs then Geo parcel document - Declared - begin editing occurs after Geo parcel document - Main - initialize
17) In 100.00% of traces in the log, If Control summary - Main - initialize occurs then Control summary - Main - begin editing occurs after Control summary - Main - initialize
18) In 100.00% of traces in the log, If Geo parcel document - Main - initialize occurs then Geo parcel document - Declared - create occurs after Geo parcel document - Main - initialize
19) In 100.00% of traces in the log, Control summary - Main - finish editing occurs if preceded by Control summary - Main - begin editing
20) In 100.00% of traces in the log, Payment application - Application - calculate occurs if preceded by Payment application - Application - begin editing
21) In 100.00% of traces in the log, Payment application - Application - begin editing occurs if preceded by Payment application - Application - initialize
22) In 100.00% of traces in the log, Geo parcel document - Declared - begin editing occurs if preceded by Geo parcel document - Main - initialize
23) In 100.00% of traces in the log, Control summary - Main - begin editing occurs if preceded by Control summary - Main - initialize
24) In 100.00% of traces in the log, Payment application - Application - mail income occurs at most once
25) In 100.00% of traces in the log, Geo parcel document - Main - initialize occurs at most once
26) In 100.00% of traces in the log, Payment application - Application - finish payment occurs at most once
27) In 100.00% of traces in the log, Control summary - Main - finish editing occurs at most once
28) In 100.00% of traces in the log, Payment application - Application - mail valid occurs at most once
29) In 100.00% of traces in the log, Department control parcels - Main - performed occurs at most once
30) In 100.00% of traces in the log, Reference alignment - Main - performed occurs at most once
31) In 100.00% of traces in the log, Control summary - Main - begin editing occurs at most once
32) In 99.99% of traces in the log, Reference alignment - Main - initialize occurs at least once
33) In 99.99% of traces in the log, Geo parcel document - Declared - finish editing occurs at least once
34) In 99.99% of traces in the log, Reference alignment - Main - performed occurs at least once
35) In 99.99% of traces in the log, If Payment application - Application - mail valid occurs then Geo parcel document - Main - initialize occurs after Payment application - Application - mail valid
36) In 99.99% of traces in the log, If Reference alignment - Main - initialize occurs then Reference alignment - Main - performed occurs after Reference alignment - Main - initialize
37) In 99.99% of traces in the log, Geo parcel document - Main - initialize occurs if preceded by Payment application - Application - mail valid
38) In 99.99% of traces in the log, Geo parcel document - Declared - finish editing occurs if preceded by Geo parcel document - Declared - begin editing
39) In 99.99% of traces in the log, Reference alignment - Main - performed occurs if preceded by Reference alignment - Main - initialize
40) In 99.99% of traces in the log, Payment application - Application - abort payment occurs at most once
41) In 99.99% of traces in the log, If Geo parcel document - Declared - begin editing occurs then Geo parcel document - Declared - finish editing occurs after Geo parcel document - Declared - begin editing
42) In 99.99% of traces in the log, If Control summary - Main - finish editing occurs then Reference alignment - Main - initialize occurs after Control summary - Main - finish editing
43) In 99.99% of traces in the log, Reference alignment - Main - initialize occurs if preceded by Control summary - Main - finish editing
44) In 99.98% of traces in the log, Department control parcels - Main - performed occurs at least once
45) In 99.97% of traces in the log, If Payment application - Application - calculate occurs then Payment application - Application - finish editing occurs after Payment application - Application - calculate
46) In 99.97% of traces in the log, If Payment application - Application - finish editing occurs then Payment application - Application - decide occurs after Payment application - Application - finish editing
47) In 99.97% of traces in the log, Payment application - Application - finish editing occurs if preceded by Payment application - Application - calculate
48) In 99.97% of traces in the log, Payment application - Application - decide occurs if preceded by Payment application - Application - finish editing
49) In 99.97% of traces in the log, Payment application - Application - begin payment occurs at least once
50) In 99.97% of traces in the log, Payment application - Application - finish payment occurs at least once
51) In 99.97% of traces in the log, If Payment application - Application - decide occurs then Payment application - Application - begin payment occurs after Payment application - Application - decide
52) In 99.95% of traces in the log, Payment application - Application - insert document occurs at least once
53) In 99.95% of traces in the log, Payment application - Application - finish payment occurs if preceded by Payment application - Application - insert document
54) In 99.86% of traces in the log, If Payment application - Application - insert document occurs then Payment application - Application - finish payment occurs after Payment application - Application - insert document
55) In 99.73% of traces in the log, If Reference alignment - Main - performed occurs then Department control parcels - Main - performed occurs after Reference alignment - Main - performed
56) In 99.73% of traces in the log, Department control parcels - Main - performed occurs if preceded by Reference alignment - Main - performed
57) In 99.73% of traces in the log, Payment application - Application - insert document occurs at most once
58) In 99.64% of traces in the log, Payment application - Application - decide occurs at least twice
59) In 99.55% of traces in the log, Payment application - Application - finish editing occurs at least twice
60) In 99.32% of traces in the log, Payment application - Application - revoke decision occurs at least once
61) In 99.32% of traces in the log, Payment application - Application - begin payment occurs if preceded by Payment application - Application - revoke decision
62) In 99.32% of traces in the log, Payment application - Application - revoke decision occurs if preceded by Payment application - Application - decide
63) In 99.27% of traces in the log, If Payment application - Application - revoke decision occurs then Payment application - Application - calculate occurs after Payment application - Application - revoke decision
64) In 99.16% of traces in the log, If Payment application - Application - begin payment occurs then Payment application - Application - insert document occurs after Payment application - Application - begin payment
65) In 99.05% of traces in the log, Payment application - Application - insert document occurs if preceded by Payment application - Application - begin payment
66) In 98.42% of traces in the log, Payment application - Application - revoke decision occurs at most once
67) In 97.74% of traces in the log, If Payment application - Application - begin editing occurs then Payment application - Application - revoke decision occurs after Payment application - Application - begin editing
68) In 93.02% of traces in the log, If Department control parcels - Main - performed occurs then Payment application - Application - initialize occurs after Department control parcels - Main - performed
69) In 93.02% of traces in the log, Payment application - Application - initialize occurs if preceded by Department control parcels - Main - performed
70) In 91.62% of traces in the log, Payment application - Application - begin editing occurs at least twice
71) In 89.26% of traces in the log, Payment application - Change - initialize does not occur
72) In 89.26% of traces in the log, Payment application - Change - begin editing does not occur
73) In 89.26% of traces in the log, Payment application - Change - calculate does not occur
74) In 88.97% of traces in the log, If Geo parcel document - Declared - finish editing occurs then Payment application - Application - begin editing occurs after Geo parcel document - Declared - finish editing
75) In 87.75% of traces in the log, Payment application - Main - insert document does not occur
76) In 87.15% of traces in the log, Control summary - Main - initialize occurs if preceded by Geo parcel document - Declared - create
77) In 86.67% of traces in the log, Department control parcels - Main - save does not occur
78) In 85.37% of traces in the log, Department control parcels - Main - begin editing does not occur
79) In 84.03% of traces in the log, If Geo parcel document - Declared - create occurs then Control summary - Main - initialize occurs after Geo parcel document - Declared - create
80) In 82.54% of traces in the log, Geo parcel document - Main - insert document does not occur
81) In 80.55% of traces in the log, Control summary - Main - save does not occur
82) In 77.09% of traces in the log, If Payment application - Application - mail income occurs then Payment application - Application - mail valid occurs after Payment application - Application - mail income
83) In 77.09% of traces in the log, Payment application - Application - mail valid occurs if preceded by Payment application - Application - mail income
84) In 71.95% of traces in the log, Geo parcel document - Declared - create occurs if preceded by Geo parcel document - Declared - finish editing
85) In 71.64% of traces in the log, Geo parcel document - Declared - create occurs at most once
86) In 71.13% of traces in the log, Reference alignment - Main - save does not occur
