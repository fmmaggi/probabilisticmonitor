Activities:
1) Payment application - Application - begin editing : Exists in 100.00% of traces in the log
2) Payment application - Application - initialize : Exists in 100.00% of traces in the log
3) Payment application - Application - calculate : Exists in 100.00% of traces in the log
4) Control summary - Main - initialize : Exists in 99.99% of traces in the log
5) Payment application - Application - mail income : Exists in 99.99% of traces in the log
6) Control summary - Main - finish editing : Exists in 99.99% of traces in the log
7) Control summary - Main - begin editing : Exists in 99.99% of traces in the log
8) Payment application - Application - mail valid : Exists in 99.99% of traces in the log
9) Reference alignment - Main - performed : Exists in 99.99% of traces in the log
10) Department control parcels - Main - performed : Exists in 99.99% of traces in the log
11) Parcel document - Main - begin editing : Exists in 99.99% of traces in the log
12) Payment application - Application - finish editing : Exists in 99.99% of traces in the log
13) Parcel document - Main - initialize : Exists in 99.99% of traces in the log
14) Reference alignment - Main - initialize : Exists in 99.99% of traces in the log
15) Payment application - Application - decide : Exists in 99.98% of traces in the log
16) Payment application - Application - finish payment : Exists in 99.98% of traces in the log
17) Payment application - Application - begin payment : Exists in 99.98% of traces in the log
18) Parcel document - Main - finish editing : Exists in 99.97% of traces in the log
19) Entitlement application - Main - calculate : Exists in 99.97% of traces in the log
20) Entitlement application - Main - begin editing : Exists in 99.97% of traces in the log
21) Entitlement application - Main - initialize : Exists in 99.97% of traces in the log
22) Entitlement application - Main - mail valid : Exists in 99.97% of traces in the log
23) Entitlement application - Main - finish editing : Exists in 99.96% of traces in the log
24) Entitlement application - Main - decide : Exists in 99.96% of traces in the log
25) Payment application - Application - abort payment : Exists in 99.43% of traces in the log
26) Parcel document - Main - insert document : Exists in 61.84% of traces in the log
27) Payment application - Application - revoke decision : Exists in 40.20% of traces in the log
28) Entitlement application - Main - save : Exists in 26.83% of traces in the log
29) Department control parcels - Main - begin editing : Exists in 20.31% of traces in the log
30) Payment application - Main - insert document : Exists in 19.60% of traces in the log
31) Entitlement application - Change - insert document : Exists in 19.29% of traces in the log
32) Department control parcels - Main - save : Exists in 18.89% of traces in the log
33) Entitlement application - Change - initialize : Exists in 16.50% of traces in the log
34) Entitlement application - Change - begin editing : Exists in 16.50% of traces in the log
35) Entitlement application - Change - calculate : Exists in 16.50% of traces in the log
36) Payment application - Change - calculate : Exists in 16.19% of traces in the log
37) Payment application - Change - begin editing : Exists in 16.19% of traces in the log
38) Payment application - Change - initialize : Exists in 16.19% of traces in the log
39) Payment application - Main - save : Exists in 15.85% of traces in the log
40) Reference alignment - Main - insert document : Exists in 12.46% of traces in the log
Constraints:
1) In 100.00% of traces in the log, Payment application - Application - initialize occurs at least once
2) In 100.00% of traces in the log, Payment application - Application - mail income occurs at most once
3) In 100.00% of traces in the log, Payment application - Application - finish payment occurs at most once
4) In 100.00% of traces in the log, Payment application - Application - begin editing occurs at least once
5) In 100.00% of traces in the log, Payment application - Application - calculate occurs at least twice
6) In 100.00% of traces in the log, Control summary - Main - finish editing occurs at most once
7) In 100.00% of traces in the log, Payment application - Application - mail valid occurs at most once
8) In 100.00% of traces in the log, Department control parcels - Main - performed occurs at most once
9) In 100.00% of traces in the log, Entitlement application - Main - initialize occurs at most once
10) In 100.00% of traces in the log, Reference alignment - Main - performed occurs at most once
11) In 100.00% of traces in the log, Parcel document - Main - begin editing occurs at most once
12) In 100.00% of traces in the log, Control summary - Main - begin editing occurs at most once
13) In 100.00% of traces in the log, Payment application - Application - initialize occurs at most once
14) In 100.00% of traces in the log, If Payment application - Application - initialize occurs then Payment application - Application - begin editing occurs after Payment application - Application - initialize
15) In 100.00% of traces in the log, Parcel document - Main - initialize occurs at most once
16) In 100.00% of traces in the log, Control summary - Main - initialize occurs at most once
17) In 100.00% of traces in the log, Reference alignment - Main - initialize occurs at most once
18) In 100.00% of traces in the log, Payment application - Application - calculate occurs if preceded by Payment application - Application - begin editing
19) In 100.00% of traces in the log, Payment application - Application - begin editing occurs if preceded by Payment application - Application - initialize
20) In 99.99% of traces in the log, Payment application - Application - mail income occurs at least once
21) In 99.99% of traces in the log, Payment application - Application - mail valid occurs at least once
22) In 99.99% of traces in the log, Control summary - Main - initialize occurs at least once
23) In 99.99% of traces in the log, Control summary - Main - begin editing occurs at least once
24) In 99.99% of traces in the log, Control summary - Main - finish editing occurs at least once
25) In 99.99% of traces in the log, If Control summary - Main - begin editing occurs then Control summary - Main - finish editing occurs after Control summary - Main - begin editing
26) In 99.99% of traces in the log, Control summary - Main - finish editing occurs if preceded by Control summary - Main - begin editing
27) In 99.99% of traces in the log, If Control summary - Main - initialize occurs then Control summary - Main - begin editing occurs after Control summary - Main - initialize
28) In 99.99% of traces in the log, Control summary - Main - begin editing occurs if preceded by Control summary - Main - initialize
29) In 99.99% of traces in the log, Payment application - Application - finish editing occurs at least once
30) In 99.99% of traces in the log, Reference alignment - Main - performed occurs at least once
31) In 99.99% of traces in the log, Parcel document - Main - initialize occurs at least once
32) In 99.99% of traces in the log, Department control parcels - Main - performed occurs at least once
33) In 99.99% of traces in the log, Parcel document - Main - begin editing occurs at least once
34) In 99.99% of traces in the log, Reference alignment - Main - initialize occurs at least once
35) In 99.99% of traces in the log, If Reference alignment - Main - initialize occurs then Reference alignment - Main - performed occurs after Reference alignment - Main - initialize
36) In 99.99% of traces in the log, If Parcel document - Main - initialize occurs then Parcel document - Main - begin editing occurs after Parcel document - Main - initialize
37) In 99.99% of traces in the log, Reference alignment - Main - performed occurs if preceded by Reference alignment - Main - initialize
38) In 99.99% of traces in the log, Parcel document - Main - begin editing occurs if preceded by Parcel document - Main - initialize
39) In 99.98% of traces in the log, Payment application - Application - finish payment occurs at least once
40) In 99.98% of traces in the log, Payment application - Application - decide occurs at least once
41) In 99.98% of traces in the log, If Payment application - Application - calculate occurs then Payment application - Application - finish editing occurs after Payment application - Application - calculate
42) In 99.98% of traces in the log, If Payment application - Application - finish editing occurs then Payment application - Application - decide occurs after Payment application - Application - finish editing
43) In 99.98% of traces in the log, If Reference alignment - Main - performed occurs then Department control parcels - Main - performed occurs after Reference alignment - Main - performed
44) In 99.98% of traces in the log, Payment application - Application - decide occurs if preceded by Payment application - Application - finish editing
45) In 99.98% of traces in the log, If Payment application - Application - begin payment occurs then Payment application - Application - finish payment occurs after Payment application - Application - begin payment
46) In 99.98% of traces in the log, Department control parcels - Main - performed occurs if preceded by Reference alignment - Main - performed
47) In 99.98% of traces in the log, Payment application - Application - begin payment occurs if preceded by Payment application - Application - decide
48) In 99.97% of traces in the log, Entitlement application - Main - initialize occurs at least once
49) In 99.97% of traces in the log, Parcel document - Main - initialize occurs if preceded by Payment application - Application - mail valid
50) In 99.97% of traces in the log, Entitlement application - Main - begin editing occurs if preceded by Entitlement application - Main - initialize
51) In 99.97% of traces in the log, Entitlement application - Main - calculate occurs if preceded by Entitlement application - Main - begin editing
52) In 99.97% of traces in the log, Parcel document - Main - finish editing occurs if preceded by Parcel document - Main - begin editing
53) In 99.97% of traces in the log, Entitlement application - Main - mail valid occurs at least twice
54) In 99.97% of traces in the log, If Entitlement application - Main - mail valid occurs then Parcel document - Main - initialize occurs after Entitlement application - Main - mail valid
55) In 99.97% of traces in the log, Parcel document - Main - initialize occurs if preceded by Entitlement application - Main - mail valid
56) In 99.96% of traces in the log, Entitlement application - Main - decide occurs at least once
57) In 99.96% of traces in the log, Entitlement application - Main - calculate occurs at least twice
58) In 99.96% of traces in the log, If Entitlement application - Main - calculate occurs then Entitlement application - Main - finish editing occurs after Entitlement application - Main - calculate
59) In 99.96% of traces in the log, Entitlement application - Main - decide occurs if preceded by Entitlement application - Main - finish editing
60) In 99.96% of traces in the log, Entitlement application - Main - finish editing occurs if preceded by Entitlement application - Main - calculate
61) In 99.95% of traces in the log, If Entitlement application - Main - finish editing occurs then Entitlement application - Main - decide occurs after Entitlement application - Main - finish editing
62) In 99.95% of traces in the log, If Control summary - Main - finish editing occurs then Parcel document - Main - finish editing occurs after Control summary - Main - finish editing
63) In 99.94% of traces in the log, If Entitlement application - Main - begin editing occurs then Entitlement application - Main - calculate occurs after Entitlement application - Main - begin editing
64) In 99.93% of traces in the log, Department control parcels - Main - performed occurs if preceded by Parcel document - Main - finish editing
65) In 99.93% of traces in the log, Entitlement application - Main - decide occurs if preceded by Payment application - Application - calculate
66) In 99.91% of traces in the log, If Control summary - Main - finish editing occurs then Reference alignment - Main - initialize occurs after Control summary - Main - finish editing
67) In 99.91% of traces in the log, Reference alignment - Main - initialize occurs if preceded by Control summary - Main - finish editing
68) In 99.90% of traces in the log, If Entitlement application - Main - initialize occurs then Payment application - Application - initialize occurs after Entitlement application - Main - initialize
69) In 99.88% of traces in the log, Payment application - Application - initialize occurs if preceded by Entitlement application - Main - calculate
70) In 99.74% of traces in the log, If Entitlement application - Main - decide occurs then Payment application - Application - calculate occurs after Entitlement application - Main - decide
71) In 99.63% of traces in the log, Entitlement application - Main - mail valid occurs if preceded by Payment application - Application - mail income
72) In 99.57% of traces in the log, If Payment application - Application - mail valid occurs then Entitlement application - Main - mail valid occurs after Payment application - Application - mail valid
73) In 99.44% of traces in the log, Payment application - Application - finish editing occurs if preceded by Entitlement application - Main - decide
74) In 99.43% of traces in the log, Payment application - Application - abort payment occurs at least once
75) In 99.43% of traces in the log, Payment application - Application - begin payment occurs at least twice
76) In 99.43% of traces in the log, If Payment application - Application - abort payment occurs then Payment application - Application - begin payment occurs after Payment application - Application - abort payment
77) In 99.43% of traces in the log, Payment application - Application - finish payment occurs if preceded by Payment application - Application - abort payment
78) In 99.43% of traces in the log, Payment application - Application - abort payment occurs if preceded by Payment application - Application - begin payment
79) In 99.41% of traces in the log, If Payment application - Application - decide occurs then Payment application - Application - abort payment occurs after Payment application - Application - decide
80) In 99.02% of traces in the log, Payment application - Application - revoke decision occurs at most once
81) In 98.58% of traces in the log, Payment application - Application - begin editing occurs at most once
82) In 98.41% of traces in the log, Entitlement application - Main - finish editing occurs at least twice
83) In 98.30% of traces in the log, Entitlement application - Main - begin editing occurs at least twice
84) In 98.08% of traces in the log, Entitlement application - Main - decide occurs at most once
85) In 96.93% of traces in the log, If Payment application - Application - begin editing occurs then Entitlement application - Main - begin editing occurs after Payment application - Application - begin editing
86) In 96.87% of traces in the log, Parcel document - Main - insert document occurs at most once
87) In 95.06% of traces in the log, If Department control parcels - Main - performed occurs then Entitlement application - Main - initialize occurs after Department control parcels - Main - performed
88) In 95.06% of traces in the log, Entitlement application - Main - initialize occurs if preceded by Department control parcels - Main - performed
89) In 94.86% of traces in the log, If Payment application - Application - mail income occurs then Payment application - Application - mail valid occurs after Payment application - Application - mail income
90) In 94.86% of traces in the log, Payment application - Application - mail valid occurs if preceded by Payment application - Application - mail income
91) In 90.89% of traces in the log, Department control parcels - Main - begin editing occurs at most once
92) In 90.15% of traces in the log, Parcel document - Main - finish editing occurs at least twice
93) In 88.02% of traces in the log, Entitlement application - Main - save occurs at most once
94) In 87.55% of traces in the log, If Parcel document - Main - begin editing occurs then Control summary - Main - initialize occurs after Parcel document - Main - begin editing
95) In 87.55% of traces in the log, Control summary - Main - initialize occurs if preceded by Parcel document - Main - begin editing
96) In 87.54% of traces in the log, Reference alignment - Main - insert document does not occur
97) In 84.15% of traces in the log, Payment application - Main - save does not occur
98) In 83.81% of traces in the log, Payment application - Change - begin editing does not occur
99) In 83.81% of traces in the log, Payment application - Change - calculate does not occur
100) In 83.81% of traces in the log, Payment application - Change - initialize does not occur
101) In 83.50% of traces in the log, Entitlement application - Change - begin editing does not occur
102) In 83.50% of traces in the log, Entitlement application - Change - calculate does not occur
103) In 83.50% of traces in the log, Entitlement application - Change - initialize does not occur
104) In 81.11% of traces in the log, Department control parcels - Main - save does not occur
105) In 80.71% of traces in the log, Entitlement application - Change - insert document does not occur
106) In 80.40% of traces in the log, Payment application - Main - insert document does not occur
