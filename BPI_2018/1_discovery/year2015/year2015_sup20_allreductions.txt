Activities:
1) Payment application - Application - begin editing : Exists in 100.00% of traces in the log
2) Payment application - Application - initialize : Exists in 100.00% of traces in the log
3) Payment application - Application - calculate : Exists in 100.00% of traces in the log
4) Control summary - Main - initialize : Exists in 99.99% of traces in the log
5) Payment application - Application - mail income : Exists in 99.99% of traces in the log
6) Control summary - Main - finish editing : Exists in 99.99% of traces in the log
7) Control summary - Main - begin editing : Exists in 99.99% of traces in the log
8) Payment application - Application - mail valid : Exists in 99.99% of traces in the log
9) Reference alignment - Main - performed : Exists in 99.99% of traces in the log
10) Department control parcels - Main - performed : Exists in 99.99% of traces in the log
11) Parcel document - Main - begin editing : Exists in 99.99% of traces in the log
12) Payment application - Application - finish editing : Exists in 99.99% of traces in the log
13) Parcel document - Main - initialize : Exists in 99.99% of traces in the log
14) Reference alignment - Main - initialize : Exists in 99.99% of traces in the log
15) Payment application - Application - decide : Exists in 99.98% of traces in the log
16) Payment application - Application - finish payment : Exists in 99.98% of traces in the log
17) Payment application - Application - begin payment : Exists in 99.98% of traces in the log
18) Parcel document - Main - finish editing : Exists in 99.97% of traces in the log
19) Entitlement application - Main - calculate : Exists in 99.97% of traces in the log
20) Entitlement application - Main - begin editing : Exists in 99.97% of traces in the log
21) Entitlement application - Main - initialize : Exists in 99.97% of traces in the log
22) Entitlement application - Main - mail valid : Exists in 99.97% of traces in the log
23) Entitlement application - Main - finish editing : Exists in 99.96% of traces in the log
24) Entitlement application - Main - decide : Exists in 99.96% of traces in the log
25) Payment application - Application - abort payment : Exists in 99.43% of traces in the log
26) Parcel document - Main - save : Exists in 71.22% of traces in the log
27) Parcel document - Main - insert document : Exists in 61.84% of traces in the log
28) Control summary - Main - save : Exists in 58.08% of traces in the log
29) Reference alignment - Main - save : Exists in 57.40% of traces in the log
30) Parcel document - Main - check : Exists in 43.31% of traces in the log
31) Payment application - Application - revoke decision : Exists in 40.20% of traces in the log
32) Entitlement application - Main - save : Exists in 26.83% of traces in the log
33) Department control parcels - Main - begin editing : Exists in 20.31% of traces in the log
34) Payment application - Main - insert document : Exists in 19.60% of traces in the log
35) Entitlement application - Change - insert document : Exists in 19.29% of traces in the log
36) Department control parcels - Main - save : Exists in 18.89% of traces in the log
37) Entitlement application - Change - initialize : Exists in 16.50% of traces in the log
38) Entitlement application - Change - begin editing : Exists in 16.50% of traces in the log
39) Entitlement application - Change - calculate : Exists in 16.50% of traces in the log
40) Payment application - Change - calculate : Exists in 16.19% of traces in the log
41) Payment application - Change - begin editing : Exists in 16.19% of traces in the log
42) Payment application - Change - initialize : Exists in 16.19% of traces in the log
43) Payment application - Main - save : Exists in 15.85% of traces in the log
44) Reference alignment - Main - insert document : Exists in 12.46% of traces in the log
Constraints:
1) In 100.00% of traces in the log, Payment application - Application - initialize occurs at least once
2) In 100.00% of traces in the log, Payment application - Application - begin editing occurs at least once
3) In 100.00% of traces in the log, Payment application - Application - calculate occurs at least twice
4) In 100.00% of traces in the log, If Payment application - Application - initialize occurs then Payment application - Application - calculate occurs after Payment application - Application - initialize
5) In 100.00% of traces in the log, Payment application - Application - mail income occurs at most once
6) In 100.00% of traces in the log, Payment application - Application - finish payment occurs at most once
7) In 100.00% of traces in the log, Control summary - Main - finish editing occurs at most once
8) In 100.00% of traces in the log, Payment application - Application - mail valid occurs at most once
9) In 100.00% of traces in the log, Department control parcels - Main - performed occurs at most once
10) In 100.00% of traces in the log, Entitlement application - Main - initialize occurs at most once
11) In 100.00% of traces in the log, Reference alignment - Main - performed occurs at most once
12) In 100.00% of traces in the log, Parcel document - Main - begin editing occurs at most once
13) In 100.00% of traces in the log, Control summary - Main - begin editing occurs at most once
14) In 100.00% of traces in the log, Payment application - Application - initialize occurs at most once
15) In 100.00% of traces in the log, Payment application - Application - calculate occurs if preceded by Payment application - Application - initialize
16) In 100.00% of traces in the log, Parcel document - Main - initialize occurs at most once
17) In 100.00% of traces in the log, Control summary - Main - initialize occurs at most once
18) In 100.00% of traces in the log, Reference alignment - Main - initialize occurs at most once
19) In 99.99% of traces in the log, Payment application - Application - mail income occurs at least once
20) In 99.99% of traces in the log, Payment application - Application - mail valid occurs at least once
21) In 99.99% of traces in the log, Control summary - Main - initialize occurs at least once
22) In 99.99% of traces in the log, Control summary - Main - begin editing occurs at least once
23) In 99.99% of traces in the log, Control summary - Main - finish editing occurs at least once
24) In 99.99% of traces in the log, If Control summary - Main - initialize occurs then Control summary - Main - begin editing occurs after Control summary - Main - initialize
25) In 99.99% of traces in the log, If Control summary - Main - begin editing occurs then Control summary - Main - finish editing occurs after Control summary - Main - begin editing
26) In 99.99% of traces in the log, Control summary - Main - finish editing occurs if preceded by Control summary - Main - initialize
27) In 99.99% of traces in the log, Reference alignment - Main - performed occurs at least once
28) In 99.99% of traces in the log, Parcel document - Main - initialize occurs at least once
29) In 99.99% of traces in the log, Department control parcels - Main - performed occurs at least once
30) In 99.99% of traces in the log, Parcel document - Main - begin editing occurs at least once
31) In 99.99% of traces in the log, Reference alignment - Main - initialize occurs at least once
32) In 99.99% of traces in the log, If Parcel document - Main - initialize occurs then Parcel document - Main - begin editing occurs after Parcel document - Main - initialize
33) In 99.99% of traces in the log, Parcel document - Main - begin editing occurs if preceded by Parcel document - Main - initialize
34) In 99.98% of traces in the log, Payment application - Application - finish payment occurs at least once
35) In 99.98% of traces in the log, If Payment application - Application - calculate occurs then Payment application - Application - finish editing occurs after Payment application - Application - calculate
36) In 99.98% of traces in the log, If Payment application - Application - finish editing occurs then Payment application - Application - decide occurs after Payment application - Application - finish editing
37) In 99.98% of traces in the log, Payment application - Application - decide occurs if preceded by Payment application - Application - finish editing
38) In 99.98% of traces in the log, Department control parcels - Main - performed occurs if preceded by Reference alignment - Main - performed
39) In 99.97% of traces in the log, Entitlement application - Main - initialize occurs at least once
40) In 99.97% of traces in the log, Entitlement application - Main - calculate occurs if preceded by Entitlement application - Main - begin editing
41) In 99.97% of traces in the log, Entitlement application - Main - begin editing occurs if preceded by Entitlement application - Main - initialize
42) In 99.97% of traces in the log, Entitlement application - Main - mail valid occurs at least twice
43) In 99.97% of traces in the log, Parcel document - Main - initialize occurs if preceded by Entitlement application - Main - mail valid
44) In 99.97% of traces in the log, If Entitlement application - Main - mail valid occurs then Parcel document - Main - initialize occurs after Entitlement application - Main - mail valid
45) In 99.96% of traces in the log, Entitlement application - Main - decide occurs at least once
46) In 99.96% of traces in the log, Entitlement application - Main - calculate occurs at least twice
47) In 99.95% of traces in the log, If Entitlement application - Main - initialize occurs then Payment application - Application - begin payment occurs after Entitlement application - Main - initialize
48) In 99.95% of traces in the log, If Entitlement application - Main - decide occurs then Payment application - Application - finish payment occurs after Entitlement application - Main - decide
49) In 99.93% of traces in the log, Department control parcels - Main - performed occurs if preceded by Parcel document - Main - finish editing
50) In 99.93% of traces in the log, Entitlement application - Main - decide occurs if preceded by Payment application - Application - calculate
51) In 99.91% of traces in the log, If Control summary - Main - finish editing occurs then Reference alignment - Main - initialize occurs after Control summary - Main - finish editing
52) In 99.91% of traces in the log, Reference alignment - Main - initialize occurs if preceded by Control summary - Main - begin editing
53) In 99.91% of traces in the log, Payment application - Application - begin editing occurs if preceded by Entitlement application - Main - calculate
54) In 99.57% of traces in the log, If Payment application - Application - mail valid occurs then Entitlement application - Main - mail valid occurs after Payment application - Application - mail valid
55) In 99.44% of traces in the log, Payment application - Application - finish editing occurs if preceded by Entitlement application - Main - decide
56) In 99.43% of traces in the log, Payment application - Application - begin payment occurs at least twice
57) In 99.43% of traces in the log, If Payment application - Application - abort payment occurs then Payment application - Application - begin payment occurs after Payment application - Application - abort payment
58) In 99.43% of traces in the log, Payment application - Application - abort payment occurs if preceded by Payment application - Application - begin payment
59) In 99.43% of traces in the log, Payment application - Application - finish payment occurs if preceded by Payment application - Application - abort payment
60) In 98.64% of traces in the log, If Payment application - Application - begin editing occurs then Entitlement application - Main - calculate occurs after Payment application - Application - begin editing
61) In 98.58% of traces in the log, Payment application - Application - begin editing occurs at most once
62) In 98.41% of traces in the log, Entitlement application - Main - finish editing occurs at least twice
63) In 98.30% of traces in the log, Entitlement application - Main - begin editing occurs at least twice
64) In 98.08% of traces in the log, Entitlement application - Main - decide occurs at most once
65) In 94.86% of traces in the log, If Payment application - Application - mail income occurs then Payment application - Application - mail valid occurs after Payment application - Application - mail income
66) In 94.86% of traces in the log, Payment application - Application - mail valid occurs if preceded by Payment application - Application - mail income
67) In 90.15% of traces in the log, Parcel document - Main - finish editing occurs at least twice
68) In 87.55% of traces in the log, Control summary - Main - initialize occurs if preceded by Parcel document - Main - begin editing
69) In 87.55% of traces in the log, If Parcel document - Main - begin editing occurs then Control summary - Main - initialize occurs after Parcel document - Main - begin editing
70) In 87.54% of traces in the log, Reference alignment - Main - insert document does not occur
71) In 84.15% of traces in the log, Payment application - Main - save does not occur
72) In 83.81% of traces in the log, Payment application - Change - begin editing does not occur
73) In 83.81% of traces in the log, Payment application - Change - calculate does not occur
74) In 83.81% of traces in the log, Payment application - Change - initialize does not occur
75) In 83.50% of traces in the log, Entitlement application - Change - begin editing does not occur
76) In 83.50% of traces in the log, Entitlement application - Change - calculate does not occur
77) In 83.50% of traces in the log, Entitlement application - Change - initialize does not occur
78) In 81.11% of traces in the log, Department control parcels - Main - save does not occur
79) In 80.71% of traces in the log, Entitlement application - Change - insert document does not occur
80) In 80.40% of traces in the log, Payment application - Main - insert document does not occur
81) In 79.69% of traces in the log, Department control parcels - Main - begin editing does not occur
82) In 78.67% of traces in the log, Payment application - Application - mail valid occurs if preceded by Entitlement application - Main - mail valid
83) In 73.17% of traces in the log, Entitlement application - Main - save does not occur
84) In 68.22% of traces in the log, Payment application - Application - begin editing occurs if preceded by Entitlement application - Main - finish editing
85) In 63.56% of traces in the log, Payment application - Application - initialize occurs if preceded by Entitlement application - Main - finish editing
86) In 61.84% of traces in the log, Parcel document - Main - insert document occurs at least once
87) In 61.74% of traces in the log, Parcel document - Main - insert document occurs if preceded by Control summary - Main - finish editing
88) In 59.80% of traces in the log, Payment application - Application - revoke decision does not occur
89) In 59.20% of traces in the log, Payment application - Application - decide occurs at most once
90) In 58.73% of traces in the log, Payment application - Application - finish editing occurs at most once
91) In 58.20% of traces in the log, Control summary - Main - initialize occurs if preceded by Parcel document - Main - finish editing
92) In 58.20% of traces in the log, Control summary - Main - begin editing occurs if preceded by Parcel document - Main - finish editing
93) In 57.40% of traces in the log, If Reference alignment - Main - performed occurs then Reference alignment - Main - save occurs after Reference alignment - Main - performed
94) In 57.26% of traces in the log, Payment application - Application - abort payment occurs at most once
95) In 56.69% of traces in the log, Parcel document - Main - check does not occur
96) In 52.73% of traces in the log, If Parcel document - Main - finish editing occurs then Entitlement application - Main - initialize occurs after Parcel document - Main - finish editing
97) In 50.26% of traces in the log, If Parcel document - Main - insert document occurs then Payment application - Application - initialize occurs after Parcel document - Main - insert document
98) In 49.85% of traces in the log, Parcel document - Main - save occurs at least twice
99) In 46.83% of traces in the log, If Parcel document - Main - finish editing occurs then Department control parcels - Main - performed occurs after Parcel document - Main - finish editing
100) In 43.63% of traces in the log, Control summary - Main - save occurs if preceded by Parcel document - Main - save
101) In 42.74% of traces in the log, Payment application - Application - abort payment occurs at least twice
102) In 42.60% of traces in the log, Reference alignment - Main - save does not occur
103) In 41.92% of traces in the log, Control summary - Main - save does not occur
104) In 41.27% of traces in the log, Payment application - Application - finish editing occurs at least twice
105) In 40.80% of traces in the log, Payment application - Application - decide occurs at least twice
106) In 40.20% of traces in the log, Payment application - Application - revoke decision occurs at least once
107) In 40.20% of traces in the log, Payment application - Application - revoke decision occurs if preceded by Payment application - Application - decide
108) In 39.95% of traces in the log, Payment application - Application - begin payment occurs if preceded by Payment application - Application - revoke decision
109) In 39.93% of traces in the log, If Payment application - Application - revoke decision occurs then Payment application - Application - abort payment occurs after Payment application - Application - revoke decision
110) In 39.44% of traces in the log, Entitlement application - Main - initialize occurs if preceded by Reference alignment - Main - save
111) In 38.95% of traces in the log, If Parcel document - Main - save occurs then Entitlement application - Main - begin editing occurs after Parcel document - Main - save
112) In 38.46% of traces in the log, Reference alignment - Main - save occurs at least twice
113) In 38.16% of traces in the log, Parcel document - Main - insert document does not occur
114) In 35.27% of traces in the log, Control summary - Main - save occurs at least twice
115) In 34.25% of traces in the log, If Entitlement application - Main - begin editing occurs then Reference alignment - Main - save occurs after Entitlement application - Main - begin editing
116) In 34.04% of traces in the log, If Reference alignment - Main - initialize occurs then Parcel document - Main - insert document occurs after Reference alignment - Main - initialize
117) In 33.92% of traces in the log, If Parcel document - Main - finish editing occurs then Reference alignment - Main - initialize occurs after Parcel document - Main - finish editing
118) In 33.88% of traces in the log, If Control summary - Main - save occurs then Entitlement application - Main - decide occurs after Control summary - Main - save
119) In 33.22% of traces in the log, If Entitlement application - Main - finish editing occurs then Parcel document - Main - finish editing occurs after Entitlement application - Main - finish editing
120) In 32.96% of traces in the log, Parcel document - Main - insert document occurs if preceded by Reference alignment - Main - initialize
121) In 32.56% of traces in the log, Parcel document - Main - check occurs if preceded by Parcel document - Main - save
122) In 31.74% of traces in the log, Entitlement application - Main - finish editing occurs if preceded by Payment application - Application - calculate
123) In 31.74% of traces in the log, If Entitlement application - Main - calculate occurs then Parcel document - Main - save occurs after Entitlement application - Main - calculate
124) In 31.74% of traces in the log, Entitlement application - Main - finish editing occurs if preceded by Payment application - Application - begin editing
125) In 30.20% of traces in the log, Department control parcels - Main - performed occurs if preceded by Control summary - Main - save
126) In 29.95% of traces in the log, Parcel document - Main - finish editing occurs if preceded by Parcel document - Main - insert document
127) In 28.88% of traces in the log, Reference alignment - Main - performed occurs if preceded by Parcel document - Main - insert document
128) In 28.78% of traces in the log, Parcel document - Main - save does not occur
129) In 27.80% of traces in the log, If Parcel document - Main - insert document occurs then Reference alignment - Main - performed occurs after Parcel document - Main - insert document
130) In 27.52% of traces in the log, If Payment application - Application - finish payment occurs then Parcel document - Main - finish editing occurs after Payment application - Application - finish payment
131) In 26.83% of traces in the log, Entitlement application - Main - save occurs at least once
132) In 26.62% of traces in the log, Parcel document - Main - finish editing occurs if preceded by Parcel document - Main - check
133) In 25.75% of traces in the log, If Parcel document - Main - check occurs then Department control parcels - Main - performed occurs after Parcel document - Main - check
134) In 23.59% of traces in the log, If Payment application - Application - decide occurs then Control summary - Main - save occurs after Payment application - Application - decide
135) In 23.40% of traces in the log, If Reference alignment - Main - save occurs then Entitlement application - Main - finish editing occurs after Reference alignment - Main - save
136) In 23.11% of traces in the log, If Payment application - Application - begin payment occurs then Control summary - Main - save occurs after Payment application - Application - begin payment
137) In 22.98% of traces in the log, Parcel document - Main - save occurs if preceded by Department control parcels - Main - performed
138) In 22.74% of traces in the log, Parcel document - Main - check occurs at least twice
139) In 22.60% of traces in the log, Entitlement application - Main - save occurs if preceded by Payment application - Application - initialize
140) In 22.55% of traces in the log, If Parcel document - Main - finish editing occurs then Entitlement application - Main - save occurs after Parcel document - Main - finish editing
141) In 22.18% of traces in the log, If Parcel document - Main - insert document occurs then Payment application - Application - revoke decision occurs after Parcel document - Main - insert document
142) In 22.04% of traces in the log, If Parcel document - Main - save occurs then Parcel document - Main - check occurs after Parcel document - Main - save
143) In 21.29% of traces in the log, Entitlement application - Main - mail valid occurs if preceded by Payment application - Application - mail valid
144) In 21.22% of traces in the log, Control summary - Main - save occurs if preceded by Reference alignment - Main - save
145) In 20.98% of traces in the log, If Reference alignment - Main - save occurs then Payment application - Application - begin editing occurs after Reference alignment - Main - save
146) In 20.43% of traces in the log, Reference alignment - Main - save occurs if preceded by Control summary - Main - save
147) In 20.31% of traces in the log, Department control parcels - Main - begin editing occurs at least once
148) In 20.31% of traces in the log, If Department control parcels - Main - performed occurs then Department control parcels - Main - begin editing occurs after Department control parcels - Main - performed
149) In 20.31% of traces in the log, Department control parcels - Main - begin editing occurs if preceded by Department control parcels - Main - performed
150) In 20.13% of traces in the log, If Department control parcels - Main - performed occurs then Parcel document - Main - insert document occurs after Department control parcels - Main - performed
